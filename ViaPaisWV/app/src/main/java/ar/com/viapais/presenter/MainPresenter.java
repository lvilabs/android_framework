package ar.com.viapais.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ar.com.viapais.app;
import ar.com.viapais.model.NotificationData;
import ar.com.viapais.views.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by fvalle on 21/03/2016.
 *
 */
public class MainPresenter {

    private static final String dominio = "viapais.com.ar";

    private final Map<String, String> customHeaders;

    private boolean pageLoaded;
    private boolean pageSaved = false;

    public void setPageSaved(boolean pageSaved) {
        this.pageSaved = pageSaved;
    }

    private MainActivity activity;

    public MainPresenter(final MainActivity activity) {
        this.activity = activity;
        customHeaders = new HashMap();
        String versionName = "";
        try {
            versionName = activity.getContext().getPackageManager().getPackageInfo(activity.getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        customHeaders.put("app-mobile-version", versionName);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void mostrarDatos(NotificationData notificationData) {

        setWebView();
        if (pageSaved) {
            activity.getSplash().setVisibility(View.INVISIBLE);
            activity.getWebView().setVisibility(View.VISIBLE);
        } else {
            this.pageLoaded = false;
            activity.getBtnReintentar().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mostrarDatos(null);
                }
            });
            activity.getReload().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.getWebView().reload();
                }
            });

            if (pageLoaded == false) {
                activity.getSplash().setVisibility(View.VISIBLE);
                ((Activity)activity.getContext()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            }

            if (notificationData == null) {
                activity.getWebView().loadUrl("http://" + dominio +"/", customHeaders);
            } else {
                activity.getWebView().loadUrl(notificationData.getLink(), customHeaders);
            }

        }
    }

    private void setWebView(){
        WebSettings webSettings = activity.getWebView().getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setAllowContentAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        activity.getTxtError().setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        activity.getWebView().setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                if (pageLoaded == false) {
                    activity.getSplash().setVisibility(View.INVISIBLE);
                }
                activity.getTxtError().setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (pageLoaded == false) {
                    activity.getSplash().setVisibility(View.INVISIBLE);
                    ((Activity)activity.getContext()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    pageLoaded = true;
                }
                activity.getWebView().setVisibility(View.VISIBLE);

                if (url.contains(dominio) && !esShareLink(url)) {
                    try {
                        ((app)activity.getContext().getApplicationContext()).trackScreenView(url);
                    }catch (Exception e){
                        e.printStackTrace();
                        Log.w("Notificacion", e.getMessage());
                    }
                }

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                Uri uri = Uri.parse(url);
                if (uri.getHost().equals(dominio) ) {
                    Log.w("domain " , uri.getHost());
                    view.loadUrl(url, customHeaders);
                    return false;
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    activity.getContext().startActivity(i);
                }
                return true;
            }
        });
    }

    public void goBack() {
        if (activity.getWebView().canGoBack()) {
            activity.getWebView().goBack();
        }else{
            activity.close();
        }
    }

    private boolean esShareLink(String link) {
        return (
                link.contains("/share") ||
                        link.contains("/pin/create/button") ||
                        link.contains("mailto:?") ||
                        link.contains("facebook.com") ||
                        link.contains("whatsapp://send?text=")
        );
    }
}
