package ar.com.viapais;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import ar.com.viapais.model.NotificationData;
import ar.com.viapais.notifications.Notification;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;

import java.util.Date;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.service.GcmService;
import io.fabric.sdk.android.Fabric;

/**
 * Created by fvalle on 28/03/2016.
 *
 */
public class app extends MultiDexApplication {


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        initNotificationsService();
        mInstance = this;
        AnalyticsTrackers.initialize(mInstance);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        initAdvertisingIdCollection();
    }

    private static app mInstance;

    public static synchronized app getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    private void initAdvertisingIdCollection(){
        try {
            // Habilitar las funciones de display.
            Tracker t = getGoogleAnalyticsTracker();
            t.enableAdvertisingIdCollection(true);
        }catch (Exception e){
            Log.e( "AdvertisingIdCollection", e.getMessage() );
        }
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                            .setDescription(
                                    new StandardExceptionParser(this, null)
                                            .getDescription(Thread.currentThread().getName(), e))
                            .setFatal(false)
                            .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }


    private void initNotificationsService(){
        try {
            AppFrameworkContext appFrameworkContext = new AppFrameworkContext(app.this);
            GcmService gcmService = appFrameworkContext.getGcmService();
            gcmService.addTopic("viapais");
            gcmService.addListener(onResultListener);
            gcmService.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d("GCM Notification", "on Broadcast Receive OK");
                }
            });
            gcmService.startService();

        } catch (LVIException e) {
            e.printStackTrace();
        }
    }

    GcmService.OnResultListener  onResultListener = new GcmService.OnResultListener() {
        @Override
        public void successByTopic(Bundle data) {
            String message = data.getString("message");
            NotificationData notificationData = new Gson().fromJson( message, NotificationData.class );
            notificationData.setDate(new Date());
            Notification.getInstance().showNotification(app.this, notificationData);
        }

        @Override
        public void successByDeviceID(Bundle data) {
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.w("GCM Notification", lviEx.getMessage());
        }
    };

}
