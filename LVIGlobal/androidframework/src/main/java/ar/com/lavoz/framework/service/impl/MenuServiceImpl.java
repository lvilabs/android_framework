package ar.com.lavoz.framework.service.impl;

import java.util.List;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.model.MenuItem;
import ar.com.lavoz.framework.service.MenuService;

/**
 * Created by Dani on 2/23/2016.
 */
public class MenuServiceImpl extends BaseService implements MenuService {
    static MenuService menuService;
    static MenuService.OnResultListener onResultListener;

    private String[] tabs;
    private List<MenuItem> menu;

    public MenuServiceImpl() {
        propertiesUtil = AppFrameworkContext.PROPERTIES_UTIL;
        menu = propertiesUtil.getStaticMenuItems();
        tabs = propertiesUtil.getTabs();
    }

    public static MenuService getInstance(AppFrameworkContext.AppKeyFramework appKey, MenuService.OnResultListener onRstListener) throws LVIAppKeyException {
        AppFrameworkContext.AppKeyFramework.checkKey(appKey);

        if (menuService == null) {
            onResultListener = onRstListener;
            menuService = new MenuServiceImpl();
        }

        return menuService;
    }

    @Override
    public void obtenerMenu() {
        onResultListener.success(menu);
    }

    @Override
    public void obtenerTabs() {
        onResultListener.success(tabs);
    }
}
