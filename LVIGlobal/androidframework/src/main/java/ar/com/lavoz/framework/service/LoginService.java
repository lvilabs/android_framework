package ar.com.lavoz.framework.service;

import ar.com.lavoz.framework.model.LoginResult;

/**
 * Servicio para realaizar las operaciones de logueo a la aplicación.
 */
public interface LoginService {

    LoginResult login(String username, String password);

    boolean logout(String token);

    boolean register(String username, String password);

    boolean clearSessions(String username);
}
