package ar.com.lavoz.framework.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dani on 2/18/2016.
 */
public class BeanUtils {
    private static final List<String> EXCLUDE_METHODS = Arrays.asList("getClass");

    public static <T1 extends Object, T2 extends Object> void copyProperties(T1 entity, T2 entity2) {
        Class<? extends Object> copy1 = entity.getClass();
        Class<? extends Object> copy2 = entity2.getClass();

        Method[] gettersAndSetters = copy1.getMethods();

        for (Method fromMethod : gettersAndSetters) {
            try {

                String fromMethodName = fromMethod.getName();

                if (fromMethodName.startsWith("get") && !EXCLUDE_METHODS.contains(fromMethodName)) {
                    copy2.getMethod(fromMethodName.replace("get", "set"), fromMethod.getReturnType()).invoke(entity2, fromMethod.invoke(entity, null));
                }
            } catch (NoSuchMethodException nex) {
                nex.printStackTrace();
            } catch (IllegalAccessException iaex) {
                iaex.printStackTrace();
            } catch (InvocationTargetException itex) {
                itex.printStackTrace();
            }
        }
    }

}
