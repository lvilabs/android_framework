package ar.com.lavoz.framework.model;

import java.io.Serializable;

/**
 * Created by fvalle on 28/10/2015.
 * Representa un objeto video de kaltura
 */
public class KalturaVideo implements Serializable {

    private String targetId;
    private String wid;
    private Long uiconf_id;
    private KalturaFlashVars flashvars;
    private Long cache_st;
    private String entry_id;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getWid() {
        return wid;
    }

    public void setWid(String wid) {
        this.wid = wid;
    }

    public Long getUiconf_id() {
        return uiconf_id;
    }

    public void setUiconf_id(Long uiconf_id) {
        this.uiconf_id = uiconf_id;
    }

    public KalturaFlashVars getFlashvars() {
        return flashvars;
    }

    public void setFlashvars(KalturaFlashVars flashvars) {
        this.flashvars = flashvars;
    }

    public Long getCache_st() {
        return cache_st;
    }

    public void setCache_st(Long cache_st) {
        this.cache_st = cache_st;
    }

    public String getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(String entry_id) {
        this.entry_id = entry_id;
    }
}
