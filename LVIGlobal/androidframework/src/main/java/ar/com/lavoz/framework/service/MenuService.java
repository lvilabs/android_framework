package ar.com.lavoz.framework.service;

import java.util.List;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.MenuItem;

/**
 * Servicio para obtener los menues disponibles.
 *
 * @author LVI Desarrollo
 */
public interface MenuService {

    /**
     *
     */
    void obtenerMenu();

    /**
     *
     */
    void obtenerTabs();


    /***
     *
     */
    interface OnResultListener {
        /**
         *
         * @param menuItems
         */
        void success(List<MenuItem> menuItems);

        /**
         *
         * @param tabs
         */
        void success(String[] tabs);

        /**
         *
         * @param lviEx
         */
        void failure(LVIServiceException lviEx);
    }
}
