package ar.com.lavoz.framework.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Properties;

/**
 * Created by Dani on 3/22/2016.
 */
public class PropertiesBase {
    protected Properties properties;
    protected Context context;

    public void loadProperties(String fileName, Context context) {
        properties = new Properties();
        try {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(fileName);
            properties.load(inputStream);
            this.context = context;
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public String get(String codename) {
        String url = null;

        try {
            url = URLDecoder.decode(properties.getProperty(codename), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return url;
    }

}
