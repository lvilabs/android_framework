package ar.com.lavoz.framework.service;

import com.facebook.AccessToken;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.FacebookFeed;

/**
 * Created by fvalle on 17/02/2016.
 * Servicio para obtener datos de la API de facebook
 */
public interface FacebookService {

    /**
     * Obtengo el feed
     */
    void obtenerFeed(AccessToken token, String query, String fields);

    /***
     *
     */
    interface OnResultListener {
        void success(FacebookFeed unFeed);

        void failure(LVIServiceException lviEx);
    }

}
