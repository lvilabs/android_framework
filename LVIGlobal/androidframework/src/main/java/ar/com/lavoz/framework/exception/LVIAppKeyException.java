package ar.com.lavoz.framework.exception;

/**
 * Excepciones relacionadas a la llave para los contextos. La idea
 * es evitar el acceso directo a las implementaciones de los servicios.
 *
 * @author LVI Desarrollo
 */
public class LVIAppKeyException extends LVIException {
    public LVIAppKeyException(String detailMessage) {
        super(detailMessage);
    }
}
