package ar.com.lavoz.framework.util;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;

import com.facebook.AccessToken;

import java.util.Date;
import java.util.StringTokenizer;

import ar.com.lavoz.framework.R;


/**
 * Created by fvalle on 19/10/2015.
 * Utiles
 */
public class GeneralUtil {

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static String getTidPorTag(String tagNombre) {
        String tid = "";

        switch (tagNombre) {
            case "Actualidad":
                tid = "10206";
                break;
            case "Agricultura":
                tid = "10203";
                break;
            case "Ganadería":
                tid = "10204";
                break;
            case "Entrevistas":
                tid = "264053";
                break;
            case "Tecnología":
                tid = "10202";
                break;
            case "Clima":
                tid = "10205";
                break;
        }

        return tid;
    }

    public static Date getTodayAtZero() {
        long millisInDay = 60 * 60 * 24 * 1000;
        long currentTime = new Date().getTime();
        long dateOnly = (currentTime / millisInDay) * millisInDay;
        return new Date(dateOnly);
    }

    public static AccessToken getFbAppToken(Context context) {

        return new AccessToken(
                context.getString(R.string.facebook_app_token),
                context.getString(R.string.facebook_app_id),
                context.getString(R.string.facebook_user_id),
                null,
                null,
                null,
                null,
                null);
    }

    public static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }

    public static void openUrl(String url, Context context) {
        final Intent intent = new Intent(Intent.ACTION_VIEW)
                .setData(Uri.parse(url));
        context.startActivity(intent);
    }

    @SuppressWarnings("deprecation")
    public static final Drawable getDrawable(Context context, int id) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    @SuppressWarnings("deprecation")
    public static final int getColor(Context context, int id) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static int resolveSimpleOperation(String aProp) {
        int result = 1;

        StringTokenizer tokens = new StringTokenizer(aProp, "*");
        while (tokens.hasMoreTokens()) {
            result *= Integer.parseInt(tokens.nextToken().trim());
        }

        return result;
    }

}
