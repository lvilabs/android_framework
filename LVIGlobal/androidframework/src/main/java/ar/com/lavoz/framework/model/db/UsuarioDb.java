package ar.com.lavoz.framework.model.db;

import com.orm.SugarRecord;

/**
 * Created by desarrollo on 18/2/16.
 */
public class UsuarioDb extends SugarRecord {

    private String nombre;
    private String apellido;
    private String email;
    private String direccion;
    private String telefono;
    private String sexo;

    public UsuarioDb() {

    }

    public UsuarioDb(String nombre, String apellido, String email, String direccion, String telefono, String sexo) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.direccion = direccion;
        this.telefono = telefono;
        this.sexo = sexo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
