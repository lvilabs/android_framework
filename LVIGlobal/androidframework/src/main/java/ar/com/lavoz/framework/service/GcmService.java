package ar.com.lavoz.framework.service;

import android.content.BroadcastReceiver;
import android.os.Bundle;

import ar.com.lavoz.framework.exception.LVIServiceException;

/**
 * Servicio de mensajeria GCM.
 *
 * @author LVI Desarrollo
 */
public interface GcmService {

    /**
     * Inicia el servicio android.
     */
    void startService();


    /**
     * Para el servicio android.
     */
    void stopService();

    /**
     * Registra el servicio en la plataforma android.
     *
     * @param receiver un receiver.
     */
    void registerReceiver(BroadcastReceiver receiver);

    /**
     * Agrega una nuevo topico o cola a los escuchados
     * por el servicio.
     *
     * @param topic un topico o cola.
     */
    void addTopic(String topic);

    /**
     * Elimina un topico o cola.
     *
     * @param topic un topico o cola.
     */
    void removeTopic(String topic);


    /**
     * Agrega un nuevo oyente a la lista de oyentes del servicio.
     *
     * @param onResultListener un oyente.
     */
    void addListener(OnResultListener onResultListener);


    /**
     * Posibles respuestas del servicio GCM
     */
    interface OnResultListener {
        /**
         * Llamado cuando el servicio obtiene un mensaje
         * de algun topico/cola cargado.
         *
         * @param data un bundle.
         */
        void successByTopic(Bundle data);

        /**
         * Llamado cuando el servicio obtiene un mensaje
         * exclusivo por id de dispositivo.
         *
         * @param data un bundle.
         */
        void successByDeviceID(Bundle data);

        /**
         * Llamado cuando el servicio retorna un error.
         *
         * @param lviEx una excepcion con el mensaje de error.
         */
        void failure(LVIServiceException lviEx);
    }
}
