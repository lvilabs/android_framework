package ar.com.lavoz.framework.model;

/**
 * Created by fvalle on 03/11/2015.
 * Representa un item del menu
 */
public class MenuItem {

    public static final int TIPO_ACTIVITY = 0;
    public static final int TIPO_SEPARADOR = 1;
    public static final int TIPO_PORTADILLA = 2;

    /**
     * @param type         el tipo de vista a cargar.
     * @param title  The id of the string resource of the text of the item.
     * @param icon The id of the drawable resource of icon of the item.
     * @param activity     The Activity class to be loaded upon selecting the item.
     */
    public MenuItem(int type, String title, String icon, Class activity) {
        this.type = type;
        this.title = title;
        this.icon = icon;
        this.activity = activity;
    }

    /**
     * @param type         el tipo de vista a cargar.
     * @param title  The id of the string resource of the text of the item.
     * @param icon The id of the drawable resource of icon of the item.
     * @param activity     The Activity class to be loaded upon selecting the item.
     * @param tid  The id of the string resource of the text of the item.
     */
    public MenuItem(int type, String title, String icon, Class activity, String tid) {
        this.type = type;
        this.title = title;
        this.icon = icon;
        this.activity = activity;
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "title='" + title + '\'' +
                ", icon='" + icon + '\'' +
                ", activity=" + activity +
                ", tid='" + tid + '\'' +
                ", type=" + type +
                '}';
    }

    /**
     * The id of the string resource of the text of the item.
     */
    public String title;

    /**
     * The id of the drawable resource of icon of the item.
     */
    public String icon;

    /**
     * The Fragment to be loaded upon selecting the item.
     */
    public Class activity;

    /**
     * El Id de tag si es una portadilla.
     */
    public String tid;

    /**
     * El tipo de item que debe cargar
     */
    public int type;
}
