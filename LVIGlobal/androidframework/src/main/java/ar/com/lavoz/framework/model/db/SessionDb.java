package ar.com.lavoz.framework.model.db;

import com.orm.SugarRecord;

/**
 * Representa una session de la aplicación.
 */
public class SessionDb extends SugarRecord {
    private String username;
    private String token;

    public SessionDb() {
    }

    public SessionDb(String username, String token) {
        this.username = username;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
