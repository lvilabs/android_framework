package ar.com.lavoz.framework.service.impl;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.model.db.PreferenciaDb;
import ar.com.lavoz.framework.model.dto.Preferencia;
import ar.com.lavoz.framework.service.PreferenciaService;
import ar.com.lavoz.framework.util.BeanUtils;

/**
 * Implementacion del servicio de preferencias.-
 */
public class PreferenciaServiceImpl extends BaseService implements PreferenciaService {
    public static final String LOG_TAG = "PREFERENCIASERVICE:";
    private static final List<String> PREFERENCIAS;
    static PreferenciaService preferenciaService;

    static {
        // Preferencias básicas y por defecto.
        PREFERENCIAS = Arrays.asList(
                "Ciudadanos",
                "Sucesos",
                "Política",
                "Negocios",
                "Internacionales",
                "Tecnología",
                "Campo",
                "Temas",
                "Opinión");
    }

    private PreferenciaServiceImpl() {
        propertiesUtil = AppFrameworkContext.PROPERTIES_UTIL;
    }

    public static PreferenciaService getInstance(AppFrameworkContext.AppKeyFramework appKey) throws LVIAppKeyException {
        AppFrameworkContext.AppKeyFramework.checkKey(appKey);

        if (preferenciaService == null) {
            preferenciaService = new PreferenciaServiceImpl();
        }

        return preferenciaService;
    }

    @Override
    public void inicializarPreferencias() {
        //limpiarTablaCompleta(); // Solo para prueba.

        for (String preferencia : PREFERENCIAS) {
            Log.i(LOG_TAG, "existe " + preferencia + "? ...");
            if (!existePreferencia(preferencia)) {
                Log.i(LOG_TAG, "grabando ... " + preferencia);
                new PreferenciaDb(preferencia).save();
            }
        }
    }

    @Override
    public List<Preferencia> obtenerPreferencias() {
        return getPreferenciasMapeadas(PreferenciaDb.listAll(PreferenciaDb.class));
    }

    @Override
    public long grabar(Preferencia unaPreferencia) {
        try {
            PreferenciaDb db;
            if (unaPreferencia.getId() != null && unaPreferencia.getId() > 0) {
                db = PreferenciaDb.findById(PreferenciaDb.class, unaPreferencia.getId());
            } else {
                db = new PreferenciaDb();
            }

            BeanUtils.copyProperties(unaPreferencia, db);

            return db.save();
        } catch (Exception e) {
            e.printStackTrace();
            return -1l;
        }
    }

    @Override
    public void grabar(List<Preferencia> preferencias) {
        for (Preferencia unaPreferencia : preferencias) {
            grabar(unaPreferencia);
        }
    }

    @Override
    public long grabar(String nombre, boolean valor) {
        long result = -1;

        List<PreferenciaDb> preferencias = PreferenciaDb.find(PreferenciaDb.class, "nombre = ?", nombre);
        if (!preferencias.isEmpty()) {
            PreferenciaDb pDb = preferencias.get(0);
            pDb.setValor(valor);
            result = pDb.save();
        }

        return result;
    }

    private boolean existePreferencia(String unNombre) {
        return !PreferenciaDb.find(PreferenciaDb.class, "nombre = ?", unNombre).isEmpty();
    }

    private List<Preferencia> getPreferenciasMapeadas(List<PreferenciaDb> preferenciaDbs) {
        List<Preferencia> preferencias = new ArrayList<Preferencia>();

        for (PreferenciaDb preferenciaDb : preferenciaDbs) {
            Preferencia toPref = new Preferencia();
            BeanUtils.copyProperties(preferenciaDb, toPref);
            preferencias.add(toPref);
        }

        return preferencias;
    }

    private void limpiarTablaCompleta() {
        List<PreferenciaDb> preferencias = PreferenciaDb.listAll(PreferenciaDb.class);
        for (PreferenciaDb preferencia : preferencias) {
            preferencia.delete();
        }
    }

}
