package ar.com.lavoz.framework.exception;

/**
 * Excepciones generales del framework.
 *
 * @author LVI Desarrollo
 */
public class LVIException extends Exception {
    public LVIException() {
        super();
    }

    public LVIException(String detailMessage) {
        super(detailMessage);
    }

    public LVIException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public LVIException(Throwable throwable) {
        super(throwable);
    }
}
