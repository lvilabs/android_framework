package ar.com.lavoz.framework.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by fvalle on 04/11/2015.
 * Representa un listado de Posts de facebook
 */
public class FacebookFeed implements Serializable {

    private ArrayList<FacebookPost> data;

    public ArrayList<FacebookPost> getData() {
        return data;
    }

    public void setData(ArrayList<FacebookPost> data) {
        this.data = data;
    }
}
