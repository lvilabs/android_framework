package ar.com.lavoz.framework.cache;

import android.content.Context;
import android.util.Log;

import com.vincentbrison.openlibraries.android.dualcache.lib.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCacheBuilder;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCacheContextUtils;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCacheLogUtils;
import com.vincentbrison.openlibraries.android.dualcache.lib.SizeOf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Clase que implementa el cache para el framework, haciendo uso de la libreria
 * Easy-Cache (Vincent Brison - @see <a href="https://github.com/vincentbrison/android-easy-cache">https://github.com/vincentbrison/android-easy-cache</a>).
 * La cual implementa una cacha en memoria y disco.
 *
 * @param <T> tipo del objeto a cachear.
 *
 * @author LVI Desarrollo
 */
public class LVICache<T> {
    DualCache<InternalCache> cache;

    private static final String TAG_LOG = "|| LVICACHE ||";
    private static final String CACHE_NAME = "app_framework_cache";
    private static final long DEFAULT_MAX_TIMEOUT = 60 * 1000 * 30; // 30 minutos
    private static final int TEST_APP_VERSION = 1;
    private static final int RAM_MAX_SIZE = 5 * 1024 * 1024; // 5 mb
    private static final int DISK_MAX_SIZE = 15 * 1024 * 1024; //15 mb

    /**
     * Constructor para la cache.
     *
     * @param context un android context.
     * @param clazz   clase que se quiere cachear.
     */
    public LVICache(Context context, Class<T> clazz) {
        initializeCache(context);

        cache = new DualCacheBuilder<>(CACHE_NAME, TEST_APP_VERSION, InternalCache.class)
                .useReferenceInRam(RAM_MAX_SIZE, new SizeOf<InternalCache>() {
                    @Override
                    public int sizeOf(InternalCache object) {
                        try {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(baos);
                            oos.writeObject(object);
                            oos.flush();
                            byte[] buf = baos.toByteArray();
                            return buf.length;
                        } catch (IOException e) {
                            Log.e("some prob", "error in calculating date size for caching", e);
                        }
                        return sizeOf(object);
                    }
                })
                .useDefaultSerializerInDisk(DISK_MAX_SIZE, true);
    }

    /**
     * Inicializa las configuraciones internas del cache.
     *
     * @param context un android context.
     */
    private void initializeCache(Context context) {
        DualCacheLogUtils.enableLog();
        DualCacheContextUtils.setContext(context);
    }

    /**
     * Mete un objeto a la cache.
     *
     * @param key clave para el objeto cacheado.
     * @param obj objeto a cachear.
     */
    public void set(String key, T obj) {
        cache.put(key, new InternalCache<>(obj, DEFAULT_MAX_TIMEOUT));
    }

    /**
     * Mete un objeto a la cache permitiendo especificar el tiempo
     * de vida de dicho objeto.
     *
     * @param key     una clave para el objeto cacheable.
     * @param obj     objeto a cachear.
     * @param timeout tiempo de vida esperado del objeto en cache.
     */
    public void set(String key, T obj, long timeout) {
        cache.put(key, new InternalCache<>(obj, timeout));
    }

    /**
     * Obtiene un objeto desde la cache usando su clave.
     *
     * @param key clave a recuperar.
     * @return un objeto cacheado anteriormente.
     */
    public T get(String key) {
        T result = null;
        long currentTimestamp = new Date().getTime();
        InternalCache<T> iCache = cache.get(key);

        if (iCache != null) {
            Log.d(TAG_LOG, "------------------------------------------------------------------------");
            Log.d(TAG_LOG, " L V I C A C H E -------------------------------------------------------");
            Log.d(TAG_LOG, "------------------------------------------------------------------------");
            Log.d(TAG_LOG, " - KEY : " + key);
            Log.d(TAG_LOG, " - currentTimestamp : " + currentTimestamp);
            Log.d(TAG_LOG, " - OBJ timestamp : " + iCache.getTimestamp());
            Log.d(TAG_LOG, " - OBJ timeout : " + iCache.getTimeout());
            Log.d(TAG_LOG, " - Diff : " + (currentTimestamp - iCache.getTimestamp()));
            if ((currentTimestamp - iCache.getTimestamp()) > iCache.getTimeout()) {
                Log.d(TAG_LOG, " - DELETE KEY -----> " + key);
                cache.delete(key);
            } else {
                result = iCache.getContent();
            }
            Log.d(TAG_LOG, "------------------------------------------------------------------------");
        }

        return result;
    }

    /**
     * Elimina el objeto de la cache asociado a la clave.
     *
     * @param key clave del objeto a eliminar.
     */
    public void delete(String key) {
        cache.delete(key);
    }

}

/**
 * Wrapper para los objetos que serán cacheados desde los servicios.
 * Esta clase envuelve los objetos cacheables permitiendo medir su
 * tiempo de vida en la cache.
 *
 * @param <T> tipo del objeto a cachear.
 *
 * @author LVI Desarrollo
 */
class InternalCache<T> implements Serializable {
    private long timestamp;
    private long timeout;
    private T content;

    public InternalCache() {
        this.timestamp = new Date().getTime();
    }

    public InternalCache(T content, long timeout) {
        this();
        this.content = content;
        this.timeout = timeout;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public long getTimeout() {
        return timeout;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
