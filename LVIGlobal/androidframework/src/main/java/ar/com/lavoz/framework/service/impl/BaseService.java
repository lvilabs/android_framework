package ar.com.lavoz.framework.service.impl;

import ar.com.lavoz.framework.rest.HttpClient;
import ar.com.lavoz.framework.util.PropertiesUtil;

/**
 * Created by Dani on 2/15/2016.
 */
public abstract class BaseService {
    protected static final String LOG_TAG = "||(LVI) BASESERVICE:";
    protected static HttpClient HTTP_CLIENT = new HttpClient();
    protected PropertiesUtil propertiesUtil;

}
