package ar.com.lavoz.framework.rest;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by Dani on 2/4/2016.
 */
public class HttpClient {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType FORM = MediaType.parse("application/x-www-form-urlencoded");

    private static final String LOG_TAG = "||(LVI) HTTPCLIENT:";
    private final OkHttpClient baseClient = new OkHttpClient();
    private OkHttpClient clientWith30sTimeout;

    public HttpClient() {
        this.clientWith30sTimeout = baseClient.newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    public void get(String url, final IHttpResult iResult) {
        this.get(url, iResult, null);
    }

    public void get(String url, final IHttpResult iResult, final String callerKey) {
        get(url, iResult, callerKey, false);
    }

    public void get(String url, final IHttpResult iResult, final String callerKey, boolean generateHttpFailure) {

        try {
            Log.i(LOG_TAG, "METHOD=GET -> URL:" + url);

            // ---------------------------------------------------------------------------
            // TODO: Eliminar al finalizar el desarrollo, solo con fines de pruebas.
            // ---------------------------------------------------------------------------
            if (generateHttpFailure) {
                url = url.replace("interior", "exterior");
                Log.i(LOG_TAG, "METHOD=POST -> GENERATE HTTP FAILURE ----> URL:" + url);
            }
            // ---------------------------------------------------------------------------

            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            Log.i(LOG_TAG, "----> CLIENT TIMEOUT:" + clientWith30sTimeout.connectTimeoutMillis());

            clientWith30sTimeout.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    iResult.onFailure(e, callerKey);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    logResponse(response);
                    JSONObject jsonResponse = convertToJSON(response);
                    if (jsonResponse != null) {
                        iResult.onResponse(jsonResponse);
                    } else {
                        iResult.onFailure(new IOException("La respuesta http es nula"), callerKey);
                    }
                }
            });
        } catch (Exception e) {
            iResult.onFailure(new IOException(e), callerKey);
        }
    }

    public void post(String url, String json, final IHttpResult iResult) {
        try {
            Log.i(LOG_TAG, "METHOD=POST -> URL:" + url);
            RequestBody body = RequestBody.create(JSON, json);

            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            baseClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    iResult.onFailure(e, null);
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    iResult.onResponse(convertToJSON(response));
                }
            });
        } catch (Exception ex) {
            iResult.onFailure(new IOException(ex), null);
        }
    }

    public Response getSyncro(String url) {
        Response httpResponse = null;

        try {
            Log.i(LOG_TAG, "METHOD=GET SYNCRO -> URL:" + url);

            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();

            httpResponse = baseClient.newCall(request).execute();
            logResponse(httpResponse);
        } catch (Exception ex) {
            Log.e(LOG_TAG, "GET SYNCRO ERROR -> URL:" + ex.getMessage());
        }

        return httpResponse;
    }

    public Response postSyncro(String url, Map<String, String> body) {
        Response httpResponse = null;

        try {
            Log.i(LOG_TAG, "METHOD=POST SYNCRO -> URL:" + url);

            FormBody.Builder fBody = new FormBody.Builder();
            for (Map.Entry<String, String> entry : body.entrySet()) {
                fBody.add(entry.getKey(), entry.getValue());
            }

            Request request = new Request.Builder()
                    .url(url)
                    .post(fBody.build())
                    .build();

            httpResponse = baseClient.newCall(request).execute();
            logResponse(httpResponse);
        } catch (Exception ex) {
            Log.e(LOG_TAG, "POST SYNCRO ERROR -> URL:" + ex.getMessage());
        }

        return httpResponse;
    }

    private void logResponse(Response response) throws IOException {
        Log.d(LOG_TAG, "----> RESPONSE.FROM:" + response.request().url().toString());
        Log.d(LOG_TAG, "----> RESPONSE.BODY.CONTENT_TYPE:" + response.body().contentType());
        Log.d(LOG_TAG, "----> RESPONSE.BODY.CONTENT_LENGTH:" + response.body().contentLength());
        Log.d(LOG_TAG, "----> RESPONSE.MESSAGE:" + response.message());
        Log.d(LOG_TAG, "----> RESPONSE.ISSUCCESSFUL:" + response.isSuccessful());
    }

    public interface IHttpResult {
        // Bandera general para deferenciar llamadas similares.
        public Map<String, String> params = new HashMap<String, String>();

        void onResponse(JSONObject result);

        void onFailure(IOException ex, String callerKey);
    }

    private JSONObject convertToJSON(Response response) {
        JSONObject result = null;

        try {
            result = new JSONObject(response.body().string());
        } catch (JSONException jex) {
            Log.e(LOG_TAG, " :: convertToJSON: " + jex.getMessage());
        } catch (Exception ex) {
            Log.e(LOG_TAG, " :: convertToJSON: " + ex.getMessage());
        }

        return result;
    }

}
