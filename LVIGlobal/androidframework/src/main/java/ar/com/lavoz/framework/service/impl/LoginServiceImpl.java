package ar.com.lavoz.framework.service.impl;

import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.model.LoginResult;
import ar.com.lavoz.framework.model.db.SessionDb;
import ar.com.lavoz.framework.service.LoginService;
import ar.com.lavoz.framework.util.JsonUtil;
import okhttp3.Response;

/**
 * Implementación del servicio de login para la aplicación.
 */
public class LoginServiceImpl extends BaseService implements LoginService {
    static final String DEFAULT_PASSWORD = "";
    public static final String LOG_TAG = "LOGINSERVICE:";
    static LoginService loginService;

    private LoginServiceImpl() {
        propertiesUtil = AppFrameworkContext.PROPERTIES_UTIL;
    }

    public static LoginService getInstance(AppFrameworkContext.AppKeyFramework appKey) throws LVIAppKeyException {
        AppFrameworkContext.AppKeyFramework.checkKey(appKey);

        if (loginService == null) {
            loginService = new LoginServiceImpl();
        }

        return loginService;
    }

    @Override
    public LoginResult login(String username, String password) {
        LoginResult loginResult = null;

        if (password == null) {
            password = DEFAULT_PASSWORD;
        }

        try {
            Response response = HTTP_CLIENT.postSyncro(propertiesUtil.getLoginUrl(), getFormParams(username, password));
            if (response.isSuccessful()) {
                String bodyResponse = response.body().string();
                Log.d(LOG_TAG, "LOGIN - Login Result: " + bodyResponse);
                loginResult = new JsonUtil<LoginResult>().fromJson(bodyResponse, LoginResult.class);

                SessionDb currentSession = new SessionDb(username, password);
                List<SessionDb> sessions = SessionDb.find(SessionDb.class, "username = ?", username);
                if(!sessions.isEmpty()) {
                    Log.d(LOG_TAG, sessions.size() + " sessiones encontradas ...");
                    currentSession = sessions.get(0);
                }

                currentSession.setToken(loginResult.getResponse().getToken());
                currentSession.save();
                Log.d(LOG_TAG, "Session grabada ...");
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "LOGIN - Error: " + e.getMessage());
            e.printStackTrace();
        }

        return loginResult;
    }

    @Override
    public boolean logout(String token) {
        Response response = HTTP_CLIENT.getSyncro(propertiesUtil.getLogoutUrl(token));

        if(response.isSuccessful()) {
            int sNum = SessionDb.deleteAll(SessionDb.class, "token=?", token);
            Log.d(LOG_TAG, sNum + " sessiones eliminadas ...");
        }

        return response.isSuccessful();
    }

    @Override
    public boolean register(String username, String password) {
        if (password == null) {
            password = DEFAULT_PASSWORD;
        }

        Response response = HTTP_CLIENT.postSyncro(propertiesUtil.getRegisterUrl(), getFormParams(username, password));

        if(response.isSuccessful()) {
            SessionDb currentSession = new SessionDb(username, password);
            currentSession.save();
            Log.d(LOG_TAG, "Session grabada ...");

        }
        return response.isSuccessful();
    }

    private Map<String, String> getFormParams(String username, String password) {
        Map<String, String> formParams = new HashMap<>();
        formParams.put("username", username);
        formParams.put("password", password);

        return formParams;
    }

    public boolean clearSessions(String username) {
        boolean result = true;

        List<SessionDb> sessions = SessionDb.find(SessionDb.class, "username = ?", username);
        for (SessionDb sDB : sessions) {
            result = result && sDB.delete();
        }

        return result;
    }
}
