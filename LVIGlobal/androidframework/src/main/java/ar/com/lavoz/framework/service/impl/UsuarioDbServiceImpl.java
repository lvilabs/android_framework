package ar.com.lavoz.framework.service.impl;

import java.util.ArrayList;
import java.util.List;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.model.db.UsuarioDb;
import ar.com.lavoz.framework.model.dto.Usuario;
import ar.com.lavoz.framework.service.UsuarioDbService;
import ar.com.lavoz.framework.util.BeanUtils;

/**
 * Created by desarrollo on 18/2/16.
 */
public class UsuarioDbServiceImpl extends BaseService implements UsuarioDbService {

    static UsuarioDbService usuarioDbService;

    private UsuarioDbServiceImpl() {
        propertiesUtil = AppFrameworkContext.PROPERTIES_UTIL;
    }

    public static UsuarioDbService getInstance(AppFrameworkContext.AppKeyFramework appKey) throws LVIAppKeyException {
        AppFrameworkContext.AppKeyFramework.checkKey(appKey);

        if (usuarioDbService == null) {
            usuarioDbService = new UsuarioDbServiceImpl();
        }

        return usuarioDbService;
    }


    @Override
    public long grabar(Usuario usuario) {
        try {
            //UsuarioDb.findById(UsuarioDb.class, (long) 1);
            UsuarioDb db;
            if (usuario.getId() != null && usuario.getId() > 0) {
                db = UsuarioDb.findById(UsuarioDb.class, usuario.getId());
            } else {
                db = new UsuarioDb();
            }

            BeanUtils.copyProperties(usuario, db);

            long result = db.save();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return -1l;
        }
    }

    @Override
    public Usuario obtener(long id) {
        Usuario result = new Usuario();
        UsuarioDb nUsuario = UsuarioDb.findById(UsuarioDb.class, id);

        if (nUsuario != null) {
            BeanUtils.copyProperties(nUsuario, result);
        }

        return result;
    }

    @Override
    public boolean borrar(long id) {
        boolean result = false;
        try {
            UsuarioDb nUsuario = UsuarioDb.findById(UsuarioDb.class, id);
            if (nUsuario != null) {
                result = nUsuario.delete();
            }
        } catch (Exception e) {
            result = false;
        } finally {
            return result;
        }
    }

    @Override
    public List<Usuario> obtenerTodos() {
        return getUsuariosMapeados(UsuarioDb.listAll(UsuarioDb.class));
    }

    @Override
    public List<Usuario> buscar(String expresion, String... args) {
        return getUsuariosMapeados(UsuarioDb.find(UsuarioDb.class, expresion, args));
    }

    private List<Usuario> getUsuariosMapeados(List<UsuarioDb> usuariosDbs) {
        List<Usuario> usuarios = new ArrayList<Usuario>();

        for (UsuarioDb fromUser : usuariosDbs) {
            Usuario toUser = new Usuario();
            BeanUtils.copyProperties(fromUser, toUser);
            usuarios.add(toUser);
        }

        return usuarios;
    }
}
