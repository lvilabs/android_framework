package ar.com.lavoz.framework.model;

import java.io.Serializable;

/**
 * Contenedor del token utilizado en el login.
 */
public class TokenResult implements Serializable {
    private String token;

    public TokenResult() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
