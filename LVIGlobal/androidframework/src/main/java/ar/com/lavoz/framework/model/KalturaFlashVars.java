package ar.com.lavoz.framework.model;

import java.io.Serializable;

/**
 * Created by fvalle on 28/10/2015.
 */
public class KalturaFlashVars implements Serializable {

    private Object akamaiHD;
    private String twoPhaseManifest;
    private String streamerType;

    public Object getAkamaiHD() {
        return akamaiHD;
    }

    public void setAkamaiHD(Object akamaiHD) {
        this.akamaiHD = akamaiHD;
    }

    public String getTwoPhaseManifest() {
        return twoPhaseManifest;
    }

    public void setTwoPhaseManifest(String twoPhaseManifest) {
        this.twoPhaseManifest = twoPhaseManifest;
    }

    public String getStreamerType() {
        return streamerType;
    }

    public void setStreamerType(String streamerType) {
        this.streamerType = streamerType;
    }
}
