package ar.com.lavoz.framework.service;

import java.util.List;

import ar.com.lavoz.framework.model.dto.Preferencia;

/**
 * Servicios disponibles para manipular las preferencias del sistema.
 */
public interface PreferenciaService {

    /**
     * Inicializa las preferencias basicas a utilizar. Osea crea en la
     * base de datos local del dispositivo las preferencias minimas.
     *  - Noticias, Sucesos, Politica, Internacionales ...
     */
    void inicializarPreferencias();

    /**
     * Obtiene la lista completa de preferencias desde la base de datos
     * @return una lista de preferencias.
     */
    List<Preferencia> obtenerPreferencias();

    /**
     * Graba o actualiza el valor de la preferencia en la base de datos.
     * @param unaPreferencia preferencia a actualizar.
     * @return el numero de preferencias modificadas.
     */
    long grabar(Preferencia unaPreferencia);

    /**
     * Graba o actualiza el valor de asociado a la preferencia.
     * @param nombre un nombre de preferencia.
     * @param valor un valor para la preferencia.
     * @return
     */
    long grabar(String nombre, boolean valor);

    /**
     * Graba o actualiza una lista completa de preferencias.
     * @param preferencias lista de preferencias.
     */
    void grabar(List<Preferencia> preferencias);

}
