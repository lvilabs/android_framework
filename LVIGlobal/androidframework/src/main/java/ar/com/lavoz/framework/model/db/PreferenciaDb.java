package ar.com.lavoz.framework.model.db;

import com.orm.SugarRecord;

/**
 * Representa una preferencias o gusto de los usuarios del sistema.
 */
public class PreferenciaDb extends SugarRecord {
    private String nombre;
    private boolean valor;
    private boolean visible;

    public PreferenciaDb() {
    }

    public PreferenciaDb(String nombre) {
        this(nombre, false);
    }

    public PreferenciaDb(String nombre, boolean valor) {
        this.nombre = nombre;
        this.valor = valor;
        this.visible = true;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }

    public boolean getVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}

