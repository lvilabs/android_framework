package ar.com.lavoz.framework.context;

import android.content.Context;
import android.util.Log;

import com.orm.SugarContext;

import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.framework.model.db.SessionDb;
import ar.com.lavoz.framework.model.db.UsuarioDb;
import ar.com.lavoz.framework.service.FacebookService;
import ar.com.lavoz.framework.service.GcmService;
import ar.com.lavoz.framework.service.LoginService;
import ar.com.lavoz.framework.service.MenuService;
import ar.com.lavoz.framework.service.PreferenciaService;
import ar.com.lavoz.framework.service.UsuarioDbService;
import ar.com.lavoz.framework.service.impl.FacebookServiceImpl;
import ar.com.lavoz.framework.service.impl.GcmServiceImpl;
import ar.com.lavoz.framework.service.impl.LoginServiceImpl;
import ar.com.lavoz.framework.service.impl.MenuServiceImpl;
import ar.com.lavoz.framework.service.impl.PreferenciaServiceImpl;
import ar.com.lavoz.framework.service.impl.UsuarioDbServiceImpl;
import ar.com.lavoz.framework.util.PropertiesUtil;

/**
 * Punto de acceso a LVI Framework, permite el acceso a los servicios
 * generales del framework.
 *
 * @author LVI Desarrollo
 */
public class AppFrameworkContext {
    private AppKeyFramework appKeyFramework;
    public static PropertiesUtil PROPERTIES_UTIL;
    public static final String LOG_TAG = "||(LVI) FRAMEWORK:";

    /***
     * Constructor del framework
     *
     * @param context un android context.
     */
    public AppFrameworkContext(Context context) throws LVIException {
        Log.d(LOG_TAG, "Creando el AppFrameworkContext ...");

        if (context == null) {
            Log.e(LOG_TAG, "Objeto [context] es nulo");
            throw new LVIException("Error inicializando el framework, el objeto [context] es nulo.");
        }

        this.appKeyFramework = new AppKeyFramework(context);

        Log.d(LOG_TAG, "Instanciando el archivo de propiedades ...");
        AppFrameworkContext.PROPERTIES_UTIL = PropertiesUtil.getInstance(context);
    }


    /**
     * Obtiene se servicio de Facebook.
     *
     * @param onResultListener un oyente para el servicio.
     * @return un FacebookService.
     */
    public FacebookService getFacebookService(FacebookService.OnResultListener onResultListener) {
        FacebookService facebookService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio FacebookService ...");
            facebookService = FacebookServiceImpl.getInstance(appKeyFramework, onResultListener);//--
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio FacebookService.");
        }

        return facebookService;
    }

    /**
     * Obtiene el servicio de usuarios.
     *
     * @return un UsuarioService.
     */
    public UsuarioDbService getUsuarioDbService() {
        UsuarioDbService usuarioDbService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio UsuarioDbService ...");
            usuarioDbService = UsuarioDbServiceImpl.getInstance(appKeyFramework);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio UsuarioDbService.");
        }

        return usuarioDbService;
    }

    /**
     * Obtiene el servicio para manipular las preferencias.
     * @return un PreferenciaService.
     */
    public PreferenciaService getPreferenciaService() {
        PreferenciaService preferenciaService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio PreferenciaService ...");
            preferenciaService = PreferenciaServiceImpl.getInstance(appKeyFramework);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio PreferenciaService.");
        }

        return preferenciaService;
    }

    /**
     * Obtiene el servicio para los menues.
     *
     * @param onResultListener un oyente para el servicio.
     * @return un MenuService.
     */
    public MenuService getMenuService(MenuService.OnResultListener onResultListener) {
        MenuService menuService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio UsuarioDbService ...");
            menuService = MenuServiceImpl.getInstance(appKeyFramework, onResultListener);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio UsuarioDbService.");
        }

        return menuService;
    }

    /**
     * Obtiene el servicio de mensajeria GCM.
     *
     * @return un GcmService.
     */
    public GcmService getGcmService() {
        GcmService gcmService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio de notificaciones GCM.");
            gcmService = GcmServiceImpl.getInstance(appKeyFramework);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio notificaciones GCM.");
        }

        return gcmService;
    }

    /***
     * Obtiene el servicio de login para la aplicación.
     *
     * @return un servicio de login.
     */
    public LoginService getLoginService() {
        LoginService nodoService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio LoginService ...");
            nodoService = LoginServiceImpl.getInstance(appKeyFramework);//--
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio LoginService.");
        }

        return nodoService;
    }

    /***
     * Inicializa el contexto para hacer operaciones contra la base de datos SQLLite.
     */
    public void sqlLiteInit() {
        SugarContext.init(appKeyFramework.getContext());

        // Hack findById function to force the create table sentence.
        SessionDb.findById(SessionDb.class, 1l);
        UsuarioDb.findById(UsuarioDb.class, 1l);
    }

    /**
     * Finaliza el contexto de SQLLite
     */
    public void sqlLiteTerminate() {
        SugarContext.terminate();
    }

    /***
     * Está clase sirve como llave para evitar accesos a los servicios
     * desde fuera del <b>AppFrameworkContext</b>.
     */
    public static class AppKeyFramework {
        private Context context;

        private AppKeyFramework(Context context) {
            this.context = context;
        }

        public Context getContext() {
            return context;
        }

        /***
         * Chequea que la llave no sea null.
         *
         * @param aKey una clave privada,
         * @throws LVIAppKeyException lanzada si la llave  no se puede chequear.
         */
        public static void checkKey(AppKeyFramework aKey) throws LVIAppKeyException {
            if (aKey == null) {
                throw new LVIAppKeyException("Se deben instanciar los servicios desde AppFrameworkContext");
            }
        }
    }

}
