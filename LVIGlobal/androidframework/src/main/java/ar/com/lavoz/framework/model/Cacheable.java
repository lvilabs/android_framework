package ar.com.lavoz.framework.model;

import java.util.Date;

/**
 * Created by fvalle on 16/10/2015.
 * Clase abstracta que representa un objeto que puede persistirse en disco
 */
public abstract class Cacheable {
    public abstract String getKey();
    public abstract int getVersion();
    public abstract Date getTimestampCache();
    public abstract void setTimestampCache(Date timestampNota);
    public abstract String getKey(String subseccion);
}
