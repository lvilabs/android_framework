package ar.com.lavoz.framework.exception;

/**
 * Representa una excepci&oacute;n en la capa de servicios del framework.
 *
 * @author LVI Desarrollo
 */
public class LVIServiceException extends LVIException {
    public LVIServiceException() {
        super();
    }

    public LVIServiceException(String detailMessage) {
        super(detailMessage);
    }

    public LVIServiceException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public LVIServiceException(Throwable throwable) {
        super(throwable);
    }
}
