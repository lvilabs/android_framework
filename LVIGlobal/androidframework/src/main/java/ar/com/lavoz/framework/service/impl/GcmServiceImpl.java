package ar.com.lavoz.framework.service.impl;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.gcm.MyGcmListenerService;
import ar.com.lavoz.framework.gcm.RegistrationIntentService;
import ar.com.lavoz.framework.service.GcmService;

/**
 * Created by Dani on 3/3/2016.
 */
public class GcmServiceImpl implements GcmService {
    private static final String LOG_TAG = "GCMSERVICE:";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String SERVICE_CLASS = "ar.com.lavoz.framework.gcm.RegistrationIntentService";
    private static GcmService gcmService;
    private final Context context;
    private Intent intent;
    private boolean isReceiverRegistered = false;

    private GcmServiceImpl(Context context) {
        this.context = context;
    }

    public static GcmService getInstance(AppFrameworkContext.AppKeyFramework appKey) throws LVIAppKeyException {
        AppFrameworkContext.AppKeyFramework.checkKey(appKey);

        if (gcmService == null) {
            gcmService = new GcmServiceImpl(appKey.getContext());
        }

        return gcmService;
    }

    public void startService() {
        if (!isServiceRunning(SERVICE_CLASS)) {
            if (checkPlayServices()) {
                Log.i(LOG_TAG, "|||||||| START GCM SERVICE ||||||||");
                // Start IntentService to register this application with GCM.
                this.intent = new Intent(context, RegistrationIntentService.class);
                context.startService(intent);
            } else {
                Log.i(LOG_TAG, "|||||||| ERROR - START GCM SERVICE ||||||||");
            }
        } else {
            Log.i(LOG_TAG, "********* GCM SERVICE WAS ALREADY STARTED *********");
        }
    }

    public void stopService() {
        if (this.intent != null) {
            context.stopService(intent);
        }
    }

    private boolean isServiceRunning(String serviceClassName) {
        Log.d(LOG_TAG, "-----> GCM SERVICE isServiceRunning() ???");
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            Log.d(LOG_TAG, "        . " + runningServiceInfo.service.getClassName());
            if (runningServiceInfo.service.getClassName().equals(serviceClassName)) {
                return true;
            }
        }
        return false;
    }

    public void registerReceiver(BroadcastReceiver receiver) {
        if (!isReceiverRegistered) {
            Log.i(LOG_TAG, "|||||||||||||||||||||| registerReceiver |||||||||||||||||||| ");
            LocalBroadcastManager.getInstance(context).registerReceiver(receiver,
                    new IntentFilter(RegistrationIntentService.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                try {
                    apiAvailability.getErrorDialog((Activity) context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                }catch (Exception ex){
                }finally {
                    Log.i(LOG_TAG, "PLAY_SERVICES_RESOLUTION_REQUEST ERROR CODE: " + resultCode);
                }

            } else {
                Log.i(LOG_TAG, "This device is not supported.");
                ((Activity) context).finish();
            }
            return false;
        }
        return true;
    }

    public void addTopic(String topic) {
        MyGcmListenerService.topics.add(topic);
    }

    public void removeTopic(String topic) {
        MyGcmListenerService.topics.remove(topic);
    }

    @Override
    public void addListener(OnResultListener onResultListener) {
        MyGcmListenerService.addListener(onResultListener);
    }
}
