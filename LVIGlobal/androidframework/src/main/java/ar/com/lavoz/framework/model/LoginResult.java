package ar.com.lavoz.framework.model;

/**
 * Objeto que representa la vuelta del servicio de login.
 */
public class LoginResult {
    private int code;
    private TokenResult response;

    public LoginResult() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public TokenResult getResponse() {
        return response;
    }

    public void setResponse(TokenResult response) {
        this.response = response;
    }
}