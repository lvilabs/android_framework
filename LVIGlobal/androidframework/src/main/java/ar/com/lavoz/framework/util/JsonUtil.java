package ar.com.lavoz.framework.util;

import com.google.gson.Gson;

/**
 * Created by Dani on 2/15/2016.
 */
public class JsonUtil<T> {
    private static final Gson GSON;

    static {
        GSON = new Gson();
    }

    public synchronized T fromJson(String json, Class<T> cType) {
        return GSON.fromJson(json, cType);
    }

    public synchronized String toJson(Object src) {
        return GSON.toJson(src);
    }
}
