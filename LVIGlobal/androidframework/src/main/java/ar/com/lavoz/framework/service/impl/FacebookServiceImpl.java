package ar.com.lavoz.framework.service.impl;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.google.gson.Gson;

import java.io.IOException;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.FacebookFeed;
import ar.com.lavoz.framework.service.FacebookService;

/**
 * Created by fvalle on 17/02/2016.
 * Implementación del servicio para obtener datos de la API de facebook
 */
public class FacebookServiceImpl extends BaseService implements FacebookService {

    static FacebookService facebookService;
    static FacebookService.OnResultListener onResultListener;

    public FacebookServiceImpl() {
        propertiesUtil = AppFrameworkContext.PROPERTIES_UTIL;
    }

    public static FacebookService getInstance(AppFrameworkContext.AppKeyFramework appKey, FacebookService.OnResultListener onRstListener) throws LVIAppKeyException {
        AppFrameworkContext.AppKeyFramework.checkKey(appKey);

        if (facebookService == null) {
            onResultListener = onRstListener;
            facebookService = new FacebookServiceImpl();
        }
        return facebookService;
    }


    @Override
    public void obtenerFeed(AccessToken token, String query, String fields) {
        GraphRequest request = new GraphRequest(
                token,
                query,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            int statusCode = response.getConnection().getResponseCode();
                            if (statusCode == 200) {
                                Gson gson = new Gson();
                                FacebookFeed result = gson.fromJson(response.getRawResponse(), FacebookFeed.class);
                                onResultListener.success(result);
                            } else {
                                Log.w("facebook service", "error al conectar a la API de facebook: " + response.getError().getErrorMessage());
                                onResultListener.failure(new LVIServiceException(response.getError().getErrorMessage()));
                            }
                        } catch (IOException e) {
                            Log.w("facebook service", e.getMessage());
                            onResultListener.failure(new LVIServiceException(e.getMessage()));
                        }
                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", fields);
        request.setParameters(parameters);

        request.executeAsync();
    }
}
