package ar.com.lavoz.framework.model.dto;

/**
 * DTO que representa una preferencia del sistema
 */
public class Preferencia {
    private Long id;
    private String nombre;
    private boolean valor;
    private boolean visible;

    public Preferencia() {
    }

    public Preferencia(Long id, String nombre, boolean valor, boolean visible) {
        this.id = id;
        this.nombre = nombre;
        this.valor = valor;
        this.visible = visible;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean getValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }

    public boolean getVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public String toString() {
        return "Preferencia{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", valor=" + valor +
                ", visible=" + visible +
                '}';
    }
}
