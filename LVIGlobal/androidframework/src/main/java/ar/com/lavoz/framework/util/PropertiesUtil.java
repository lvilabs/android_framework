package ar.com.lavoz.framework.util;

import android.content.Context;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import ar.com.lavoz.framework.model.MenuItem;

/**
 * Created by fvalle on 28/01/2016.
 */
public class PropertiesUtil extends PropertiesBase {
    private static final String FILE_NAME = "framework.properties";
    public static final String GENERAL_URL_KEY = "general.url";

    public static final String SRV_FACEBOOK_APP_TOKEN_KEY = "service.facebook.app_token";
    public static final String SRV_FACEBOOK_APP_ID_KEY = "service.facebook.app_id";
    public static final String SRV_FACEBOOK_USER_ID_KEY = "service.facebook.user_id";

    public static final String MENU_METADATA_PORTADA_KEY = "menu.metadata.portada";
    public static final String MENU_METADATA_ACTUALIDAD_KEY = "menu.metadata.actualidad";
    public static final String MENU_METADATA_AGRICULTURA_KEY = "menu.metadata.agricultura";
    public static final String MENU_METADATA_GANADERIA_KEY = "menu.metadata.ganaderia";
    public static final String MENU_METADATA_ENTREVISTAS_KEY = "menu.metadata.entrevistas";
    public static final String MENU_METADATA_TECNOLOGIA_KEY = "menu.metadata.tecnologia";
    public static final String MENU_METADATA_CLIMA_KEY = "menu.metadata.clima";
    public static final String MENU_METADATA_VIDEOS_KEY = "menu.metadata.videos";
    public static final String MENU_METADATA_TWITTER_KEY = "menu.metadata.twitter";
    public static final String MENU_METADATA_FACEBOOK_KEY = "menu.metadata.facebook";

    public static final String SRV_LOGIN_KEY = "service.login.url";
    public static final String SRV_LOGOUT_KEY = "service.logout.url";
    public static final String SRV_REGISTER_KEY = "service.register.url";

    private static PropertiesUtil propertiesUtil;
    private List<MenuItem> menuItems = new ArrayList<MenuItem>();

    private PropertiesUtil(Context context) {
        loadProperties(FILE_NAME, context);
    }

    public static PropertiesUtil getInstance(Context context) {
        if (propertiesUtil == null) {
            propertiesUtil = new PropertiesUtil(context);
        }

        return propertiesUtil;
    }

    public String getGeneralUrl() {
        return get(GENERAL_URL_KEY);
    }

    public String getFacebookAppToken() {
        return get(SRV_FACEBOOK_APP_TOKEN_KEY);
    }

    public String getFacebookAppId() {
        return get(SRV_FACEBOOK_APP_ID_KEY);
    }

    public String getFacebookUserId() {
        return get(SRV_FACEBOOK_USER_ID_KEY);
    }

    public String getLoginUrl() {
        return MessageFormat.format(get(SRV_LOGIN_KEY), propertiesUtil.getGeneralUrl());
    }

    public String getLogoutUrl(String token) {
        return MessageFormat.format(get(SRV_LOGOUT_KEY), propertiesUtil.getGeneralUrl(), token);
    }

    public String getRegisterUrl() {
        return MessageFormat.format(get(SRV_REGISTER_KEY), propertiesUtil.getGeneralUrl());
    }

    private void addToMenuItemsList(MenuItem item) {
        if (item != null) {
            menuItems.add(item);
        }
    }

    public MenuItem getMenuItem(String key) {
        MenuItem result = null;
        StringTokenizer tokens = new StringTokenizer(get(key), ",");

        int type = Integer.parseInt(tokens.nextToken().trim());
        String title = tokens.nextToken().trim();
        String icon = tokens.nextToken().trim();
        String className = tokens.nextToken().trim();
        String tid = tokens.nextToken().trim();
        boolean active = Boolean.valueOf(tokens.nextToken().trim());

        if (active) {
            try {
                result = new MenuItem(type, title, icon, Class.forName(className), tid);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public List<MenuItem> getStaticMenuItems() {
        if (menuItems.isEmpty()) {
            addToMenuItemsList(getMenuItem(MENU_METADATA_PORTADA_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_ACTUALIDAD_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_AGRICULTURA_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_GANADERIA_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_ENTREVISTAS_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_TECNOLOGIA_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_CLIMA_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_VIDEOS_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_TWITTER_KEY));
            addToMenuItemsList(getMenuItem(MENU_METADATA_FACEBOOK_KEY));
        }

        return menuItems;
    }

    public String[] getTabs() {
        String[] tabs = new String[menuItems.size()];

        for (int i = 0; i < menuItems.size(); i++) {
            tabs[i] = menuItems.get(i).title;
        }

        return tabs;
    }

}
