package ar.com.lavoz.framework.service;

import java.util.List;

import ar.com.lavoz.framework.model.dto.Usuario;

/**
 * Created by desarrollo on 18/2/16.
 *
 */
public interface UsuarioDbService {

    long grabar(Usuario usuario);

    Usuario obtener(long id);

    boolean borrar(long id);

    List<Usuario> obtenerTodos();

    List<Usuario> buscar(String expresion, String... args);

}
