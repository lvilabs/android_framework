package ar.com.lavoz.framework.gcm;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.service.GcmService;

/**
 * Oyente interno del servicios de mensajeria GCM.
 *
 * @author LVI Desarrollo
 */
public class MyGcmListenerService extends GcmListenerService {

    public static final Set<GcmService.OnResultListener> onResultListeners = new LinkedHashSet<>();
    public static final Set<String> topics = new HashSet<>();
    private static final String TAG = "MyGcmListenerService";
    private static final String TOPICS = "/topics/";
    private static final int SUCCESS_BY_TOPIC = 0;
    private static final int SUCCESS_BY_ID = 1;
    private static final int FAILURE = 2;

    public MyGcmListenerService() {
        Log.i(TAG, "(*) Inicializar propiedades de instancia...");
    }

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, ">>>>>>> onMessageReceived ...." + data.toString());
        if (onResultListeners.isEmpty()) {
            Log.d(TAG, ">>>>>>> IGNORED MESSAGE ...." + data.toString());
            applyToAllListeners(FAILURE, null, new LVIServiceException("Mensaje ignorado..."));
            return;
        }

        if (from.startsWith(TOPICS)) {
            String tName = from.replace(TOPICS, "");
            if (topics.contains(tName)) {
                Log.d(TAG, ">>>>>>> onMessageReceived .... BY TOPIC " + tName);
                applyToAllListeners(SUCCESS_BY_TOPIC, data, null);
            } else {
                Log.d(TAG, ">>>>>>> onMessageReceived .... TOPIC " + tName + " NOT EXIST ...");
            }
        } else {
            Log.d(TAG, ">>>>>>> onMessageReceived .... BY ID");
            applyToAllListeners(SUCCESS_BY_ID, data, null);
        }
    }
    // [END receive_message]

    /**
     * Aplica la llamada a la lista completa de los oyentes.
     *
     * @param type      tipo privado de respuesta.
     * @param bundle    un bundle.
     * @param exception excepcion del servicio.
     */
    private void applyToAllListeners(int type, Bundle bundle, LVIServiceException exception) {
        for (GcmService.OnResultListener listener : onResultListeners) {
            if (SUCCESS_BY_TOPIC == type) {
                listener.successByTopic(bundle);
            } else if (SUCCESS_BY_ID == type) {
                listener.successByDeviceID(bundle);
            } else if (FAILURE == type) {
                listener.failure(exception);
            }
        }
    }

    /**
     * Permite agregar oyentes al servicio.
     *
     * @param aListener un oyente GCM.
     */
    public static void addListener(GcmService.OnResultListener aListener) {
        onResultListeners.add(aListener);
    }

    /**
     * Permite remover un oyente previamente agregado.
     *
     * @param aListener un oyente GCM.
     * @return true si el oyente pudo ser removido, false en caso contrario.
     */
    public static boolean removeListener(GcmService.OnResultListener aListener) {
        return onResultListeners.remove(aListener);
    }

}
