package ar.com.lavoz.lviglobal.tests;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIException;

/**
 * Clase de utilidad para los test.
 */
public class UtilTest {
    private static final String LOG_TAG = "UTIL-TEST:";
    private static final String GCM_SEND_ENDPOINT = "https://gcm-http.googleapis.com/gcm/send";

    public static AppFrameworkContext appFrameworkContext;

    public static void inicializarFramework(Context context) {
        if (appFrameworkContext == null) {
            try {
                appFrameworkContext = new AppFrameworkContext(context);
            } catch (LVIException e) {
                Log.i(LOG_TAG, e.getMessage());
            }
        }
    }

    /**
     * Send a downstream message via HTTP JSON.
     *
     * @param destination the registration id of the recipient app.
     * @param message     the message to be sent
     * @throws IOException
     */
    public static String sendGCMMessage(String destination,
                                                String message) throws IOException {

        String json = "{ \"to\":\"" + destination + "\"," +
                "\"data\":{" +
                "       \"message\":\"" + message + "\"" +
                "}}";

        Log.i(LOG_TAG, "JSON::::" + json);

        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setHeader("Content-Type", "application/json");
        httpRequest.setHeader("Authorization", "key=AIzaSyDg2pwfnc-l95vVSxp9YmjWrTbyUJ2oOF8");
        httpRequest.doPost(GCM_SEND_ENDPOINT, json);

        if (httpRequest.getResponseCode() != 200) {
            throw new IOException("Invalid request."
                    + " status: " + httpRequest.getResponseCode()
                    + " response: " + httpRequest.getResponseBody());
        }

        JSONObject jsonResponse;
        try {
            jsonResponse = new JSONObject(httpRequest.getResponseBody());
            Log.i("Util-Test", "Send message:\n" + jsonResponse.toString(2));
        } catch (JSONException e) {
            Log.i("Util-Test", "Failed to parse server response:\n"
                    + httpRequest.getResponseBody());
        }
        return httpRequest.getResponseBody();
    }

}
