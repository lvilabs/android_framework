package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.util.concurrent.CountDownLatch;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.Portada;
import ar.com.lavoz.framework.service.PortadaService;

/**
 * Created by Dani on 2/16/2016.
 */
public class PortadaServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "PORTADASERVICETEST:";
    CountDownLatch signal = new CountDownLatch(1);

    public PortadaServiceTest() {
        super(Application.class);
    }

    public void testObtenerPortada() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        PortadaService portadaService = UtilTest.appFrameworkContext.getPortadaService(portadaServiceListener);

        portadaService.obtenerPortada("bid52", "2,5,6,12,61,96,44");
        signal.await();// Esperar la llamada.
    }

    PortadaService.OnResultListener portadaServiceListener = new PortadaService.OnResultListener() {
        @Override
        public void success(Portada unaPortada) {
            Log.i(LOG_TAG, "SUCCESS!!!");
            assertNotNull(unaPortada);

            Log.i(LOG_TAG, "PID: " + unaPortada.getApi().sid);
            Log.i(LOG_TAG, "NODOS: " + unaPortada.getNodos());

            // Esperamos 21 nodos.
            assertEquals(unaPortada.getNodos().size(), 21);

            signal.countDown();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);

            signal.countDown();
        }
    };

}
