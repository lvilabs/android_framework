package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;

import ar.com.lavoz.framework.util.PropertiesUtil;

/**
 * Created by Daniel Sandoval on 2/2/2016.
 */
public class PropertyFileTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "PROPERTYFILETEST:";

    public PropertyFileTest() {
        super(Application.class);
    }

    public void testReadPropertyFile() {
        PropertiesUtil pUtil = PropertiesUtil.getInstance(getContext());
        String generalUrl = pUtil.getGeneralUrl();

        assertEquals(generalUrl, "http://publicadord7.lavozdelinterior.net/");
    }

}
