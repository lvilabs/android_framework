package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.MenuItem;
import ar.com.lavoz.framework.service.MenuService;

/**
 * Created by Dani on 2/23/2016.
 */
public class MenuServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "MENUSERVICETEST:";
    CountDownLatch signal = new CountDownLatch(1);

    public MenuServiceTest() {
        super(Application.class);
    }

    public void testObtenerMenues() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        MenuService menuService = UtilTest.appFrameworkContext.getMenuService(menuServiceListener);

        menuService.obtenerMenu();
        signal.await();// Esperar para la primera llamada.

        // Segunda llamada.
        signal = new CountDownLatch(1);
        menuService.obtenerTabs();
        signal.await();// Esperar para la segunda llamada.
    }

    // Definimos los oyentes a cada llamada del servicio.
    MenuService.OnResultListener menuServiceListener = new MenuService.OnResultListener() {
        @Override
        public void success(List<MenuItem> menuItems) {
            Log.i(LOG_TAG, "--- SUCCESS MENUES RECIBIDOS ---");
            for (MenuItem item : menuItems) {
                Log.i(LOG_TAG, item.toString());
            }

            assertEquals(menuItems.size(), 8);
            signal.countDown();
        }

        @Override
        public void success(String[] tabs) {
            Log.i(LOG_TAG, " --- SUCCESS TABS RECIBIDOS ---");
            for (int i = 0; i < tabs.length; i++) {
                Log.i(LOG_TAG, tabs[i]);
            }

            assertEquals(tabs.length, 8);
            signal.countDown();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);
            signal.countDown();
        }
    };

}
