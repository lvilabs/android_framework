package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.util.concurrent.CountDownLatch;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.Portada;
import ar.com.lavoz.framework.model.Portadilla;
import ar.com.lavoz.framework.service.MasVistosService;
import ar.com.lavoz.framework.service.PortadaService;

/**
 * Created by fvalle on 16/02/2016.
 *
 */
public class MasVistosTest extends ApplicationTestCase<Application> {

    private static final String LOG_TAG = "MASVISTOSSERVICETEST:";
    CountDownLatch signal = new CountDownLatch(1);

    public MasVistosTest() {
        super(Application.class);
    }

    public void testObtenerMasVistos() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        MasVistosService masVistosService = UtilTest.appFrameworkContext.getMasVistosService(masVistosServiceListener);

        masVistosService.obtenerMasVistos();
        signal.await();// Esperar la llamada.
    }

    MasVistosService.OnResultListener masVistosServiceListener = new MasVistosService.OnResultListener() {
        @Override
        public void success(Portadilla unPortadilla) {
            Log.i(LOG_TAG, "SUCCESS!!!");
            assertNotNull(unPortadilla);

            Log.i(LOG_TAG, "PID: " + unPortadilla.getApi().sid);
            Log.i(LOG_TAG, "NODOS: " + unPortadilla.getNodos());

            // Esperamos mas de un nodo.
            assertTrue(unPortadilla.getNodos().size() > 1);

            signal.countDown();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);

            signal.countDown();
        }
    };
}
