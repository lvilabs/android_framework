package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.orm.SugarContext;

import ar.com.lavoz.framework.util.Constants;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ConstantsTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "CONSTANTSTEST:";

    public ConstantsTest() {
        super( Application.class );
    }

    public void test_read_const_from_framework() {
        Log.i(LOG_TAG, "Inicia test_read_const_from_framework() ...");
        String constFromFramework = Constants.CADENA_EJEMPLO_TEST;
        assertEquals(constFromFramework, "CADENA_EJEMPLO_TEST");
    }

}