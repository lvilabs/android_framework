package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.util.concurrent.CountDownLatch;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.Portadilla;
import ar.com.lavoz.framework.service.PortadillaService;

/**
 * Created by Dani on 2/16/2016.
 */
public class PortadillaServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "PORTADILLASERVICETEST:";
    CountDownLatch signal = new CountDownLatch(1);

    public PortadillaServiceTest() {
        super(Application.class);
    }

    public void testPortadilla() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        PortadillaService portadillaService = UtilTest.appFrameworkContext.getPortadillaService();

        portadillaService.setListener(portadillaServiceListener);
        portadillaService.obtenerPortadilla("10205");
        signal.await();// Esperar la llamada.

        signal = new CountDownLatch(1);
        portadillaService.obtenerVideos();
        signal.await();// Esperar la llamada.

        signal = new CountDownLatch(1);
        portadillaService.obtenerPortadillaPagina("10205", 10, 10);
        signal.await();// Esperar la llamada.
    }

    PortadillaService.OnResultListener portadillaServiceListener = new PortadillaService.OnResultListener() {
        @Override
        public void successPage(Portadilla unPortadilla) {
            Log.i(LOG_TAG, "SUCCESS PAGE!!!");
            assertNotNull(unPortadilla);

            Log.i(LOG_TAG, "SID: " + unPortadilla.getApi().sid);
            Log.i(LOG_TAG, "TITULO: " + unPortadilla.getTitulo());

            signal.countDown();
        }

        @Override
        public void success(Portadilla unPortadilla) {
            Log.i(LOG_TAG, "SUCCESS!!!");
            assertNotNull(unPortadilla);

            Log.i(LOG_TAG, "SID: " + unPortadilla.getApi().sid);
            Log.i(LOG_TAG, "TITULO: " + unPortadilla.getTitulo());

            signal.countDown();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);
            signal.countDown();
        }

    };

}
