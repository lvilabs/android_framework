package ar.com.lavoz.lviglobal.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.facebook.AccessToken;

import org.apache.commons.lang3.ObjectUtils;

import java.util.concurrent.CountDownLatch;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.model.FacebookFeed;
import ar.com.lavoz.framework.service.FacebookService;

/**
 * Created by Dani on 2/18/2016.
 */
public class FacebookServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "FACEBOOKSERVICETEST:";
    CountDownLatch signal = new CountDownLatch(1);

    public FacebookServiceTest() {
        super(Application.class);
    }

    public void testObtenerFeed() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        FacebookService facebookService = UtilTest.appFrameworkContext.getFacebookService(facebookServiceListener);

        AccessToken accessToken = new AccessToken(
                AppFrameworkContext.PROPERTIES_UTIL.getFacebookAppToken(),
                AppFrameworkContext.PROPERTIES_UTIL.getFacebookAppId(),
                AppFrameworkContext.PROPERTIES_UTIL.getFacebookUserId(),
                null,
                null,
                null,
                null,
                null);

        facebookService.obtenerFeed(accessToken, "/1445872835743361/feed", "id,message,created_time,full_picture,picture,link");
        signal.await();// Esperar la llamada.
    }

    FacebookService.OnResultListener facebookServiceListener = new FacebookService.OnResultListener() {
        @Override
        public void success(FacebookFeed datos) {
            Log.i(LOG_TAG, "SUCCESS!!!");
            assertNotNull(datos);

            Log.i(LOG_TAG, "FACEBOOK POST SIZE: " + datos.getData().size());

            signal.countDown();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);

            signal.countDown();
        }
    };

}
