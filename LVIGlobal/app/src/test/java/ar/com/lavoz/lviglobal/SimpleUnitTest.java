package ar.com.lavoz.lviglobal;

import android.util.Log;

import org.junit.Test;

import ar.com.lavoz.framework.util.Constants;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class SimpleUnitTest {
    private static final String LOG_TAG = "SimpleUnitTest";

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void readConstFromFramework() throws Exception {
        Log.i(LOG_TAG, "Inicio Operaciones de Test...");

        String constFromFramework = Constants.CADENA_EJEMPLO_TEST;
        assertEquals(constFromFramework, "CADENA_EJEMPLO_TEST");


        Log.i(LOG_TAG, "Fin Operaciones de Test...");
    }
}