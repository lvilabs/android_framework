package com.moby.lavoz.app.views.implement;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.moby.lavoz.app.R;
import com.moby.lavoz.app.model.NotificationData;
import com.moby.lavoz.app.notifications.Notification;
import com.moby.lavoz.app.presenter.MainPresenter;
import com.moby.lavoz.app.views.MainActivity;

public class MainActivityImpl extends AppCompatActivity implements MainActivity {

    private WebView webView;
    private MainPresenter presenter;
    private FrameLayout splash;
    private SwipeRefreshLayout swipeRefreshLayout;


    public MainActivityImpl()
    {

    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getWebView().saveState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setWebView((WebView) findViewById(R.id.webView));
        setSwipeRefreshLayout((SwipeRefreshLayout) findViewById(R.id.swipe_container));

        presenter = new MainPresenter(this);
        NotificationData notificationData = null;
        if (savedInstanceState != null) {
            getWebView().restoreState(savedInstanceState);
            presenter.setPageSaved(true);
        }else {

            presenter.setPageSaved(false);
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                notificationData = (NotificationData) extras.getSerializable(Notification.EXTRA_DATA);
            }
        }

        presenter.mostrarDatos(notificationData);
    }

    @Override
    public WebView getWebView() {
        return webView;
    }

    @Override
    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    @Override
    public void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    @Override
    public void setWebView(WebView webView) {
        this.webView = webView;
    }

    @Override
    public Context getContext(){
        return MainActivityImpl.this;
    }

    @Override
    public void close(){
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            presenter.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
