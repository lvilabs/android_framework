package com.moby.lavoz.app.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Debug;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.moby.lavoz.app.R;
import com.moby.lavoz.app.app;
import com.moby.lavoz.app.model.NotificationData;
import com.moby.lavoz.app.views.MainActivity;

import java.util.HashMap;
import java.util.Map;


/**
 * Presentador la la vista principal
 */
public class MainPresenter {

    private static final String dominioLaVoz = "lavoz.com.ar";
    private static final String dominioMundoD = "mundod.com.ar";
    private static final String dominioVos = "vos.com.ar";
    private static final String dominioAgrovoz = "agrovoz.com.ar";
    private static final String dominioMusa = "musaargentina.com.ar";
    private static final String dominioVoy = "voydeviaje.com.ar";
    private static final String dominioFb ="facebook.com";
    private static final String dominioGoogle ="google.com";
    private static final String userAgent = "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Mobile Safari/537.36";

    private final Map<String, String> customHeaders;

    private boolean pageLoaded;
    private boolean pageSaved = false;

    public void setPageSaved(boolean pageSaved) {
        this.pageSaved = pageSaved;
    }

    private MainActivity activity;

    public MainPresenter(final MainActivity activity) {
        this.activity = activity;
        customHeaders = new HashMap();
        String versionName = "";
        try {
            versionName = activity.getContext().getPackageManager().getPackageInfo(activity.getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {

        }
        customHeaders.put("app-mobile-version", versionName);
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void mostrarDatos(NotificationData notificationData) {

        setWebView();
        activity.getSwipeRefreshLayout().
                setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark);
        activity.getSwipeRefreshLayout().setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                activity.getWebView().reload();
                new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            activity.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }, 2000);
            }
        });

        if (!pageSaved){
            this.pageLoaded = false;
            if (notificationData == null) {
                activity.getWebView().loadUrl("http://m.lavoz.com.ar", customHeaders);
            } else {
                activity.getWebView().loadUrl(notificationData.getLink(), customHeaders);
            }
        }
    }

    private void setWebView(){
        WebSettings webSettings = activity.getWebView().getSettings();
        webSettings.setUserAgentString(userAgent);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setAllowContentAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        activity.getWebView().setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                activity.getSwipeRefreshLayout().setRefreshing(false);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (!pageLoaded) {
                    ((Activity)activity.getContext()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    pageLoaded = true;
                }
                activity.getSwipeRefreshLayout().setRefreshing(false);
                boolean dominioPropio = false;

                if(url.contains(dominioLaVoz) &&
                        url.contains(dominioMundoD)&&
                        url.contains(dominioVos)&&
                        url.contains(dominioAgrovoz)&&
                        url.contains(dominioMusa)&&
                        url.contains(dominioVoy)
                        && !esShareLink(url)) {
                    dominioPropio=true;
                }

                if(dominioPropio){
                    try {
                        ((app)(activity.getContext().getApplicationContext())).trackEvent("page","page-view",url);
                    }catch (Exception e){

                        Log.e("analitycs track event", e.getMessage());
                    }
                }

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if(error.getPrimaryError() == SslError.SSL_UNTRUSTED) {
                    handler.proceed();
                }
                else {
                    handler.cancel();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                boolean result = false;
                if((    url.contains(dominioLaVoz) || url.contains(dominioMundoD)||
                        url.contains(dominioVos)|| url.contains(dominioAgrovoz)||
                        url.contains(dominioMusa)|| url.contains(dominioVoy)||
                        url.contains(dominioFb)|| url.contains(dominioGoogle))
                        && !esShareLink(url)) {
                    url = url.equals("http://m.lavoz.com.ar/#_=_")? "http://m.lavoz.com.ar":url;
                    view.loadUrl(url, customHeaders);

                    if(!hasFileExtension(url)&&
                            !(url.contains(dominioFb)|| url.contains(dominioGoogle))) {
                        activity.getSwipeRefreshLayout().setRefreshing(true);
                        new android.os.Handler().postDelayed(
                                new Runnable() {
                                    public void run() {
                                        activity.getSwipeRefreshLayout().setRefreshing(false);
                                    }
                                }, 1500);
                    }

                    result = true;
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    activity.getContext().startActivity(i);
                }
                return result;
            }
        });
    }

    boolean hasFileExtension(String s) {
        return s.matches("^[\\w\\d\\:\\/\\.]+\\.\\w{3,4}(\\?[\\w\\W]*)?$");
    }

    public void goBack() {
        if (activity.getWebView().canGoBack()) {
            activity.getWebView().goBack();
        }else{
            activity.close();
        }
    }

    private boolean esShareLink(String link) {
        return (
                link.contains("/share") ||
                        link.contains("/pin/create/button") ||
                        link.contains("mailto:?") ||
                        link.contains("facebook.com/sharer.php") ||
                        link.contains("whatsapp://send?text=")
        );
    }
}
