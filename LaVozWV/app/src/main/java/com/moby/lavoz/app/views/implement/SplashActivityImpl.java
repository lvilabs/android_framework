package com.moby.lavoz.app.views.implement;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.moby.lavoz.app.R;
import com.moby.lavoz.app.views.SplashActivity;

/**
 * Created by fvalle on 05/12/16.
 */

public class SplashActivityImpl extends AppCompatActivity implements SplashActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Intent intent = new Intent(SplashActivityImpl.this, MainActivityImpl.class);
                        startActivity(intent);
                        finish();
                    }}, 2000);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
