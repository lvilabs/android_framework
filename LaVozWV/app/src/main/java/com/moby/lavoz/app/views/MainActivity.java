package com.moby.lavoz.app.views;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/**
 * Created by fvalle on 21/03/2016.
 *
 */
public interface MainActivity {
    WebView getWebView();
    SwipeRefreshLayout getSwipeRefreshLayout();
    void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout);
    void setWebView(WebView webView);
    Context getContext();
    void close();
}
