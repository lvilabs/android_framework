package ar.com.lavoz.noticiasservice.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;

import ar.com.lavoz.framework.cache.LVICache;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.rest.HttpClient;
import ar.com.lavoz.framework.service.impl.BaseService;
import ar.com.lavoz.framework.util.JsonUtil;
import ar.com.lavoz.noticiasservice.PortadaService;
import ar.com.lavoz.noticiasservice.context.NoticiasContext;
import ar.com.lavoz.noticiasservice.model.Portada;
import ar.com.lavoz.noticiasservice.util.NoticiaProperties;

/**
 * Created by Dani on 2/16/2016.
 */
public class PortadaServiceImpl extends BaseService implements PortadaService {

    static PortadaService portadaService;
    static PortadaService.OnResultListener onResultListener;
    LVICache<Portada> localCache;
    NoticiaProperties noticiaProperties;
    HttpClient.IHttpResult httpCallback = new HttpClient.IHttpResult() {
        @Override
        public void onResponse(final JSONObject result) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.i(LOG_TAG, "RESPONSE::::::" + result.toString());
                    Portada portada = new JsonUtil<Portada>().fromJson(result.toString(), Portada.class);

                    if (noticiaProperties.getPortadaCacheEnable()) {
                        Log.i(LOG_TAG, "SET CACHE .... key:: " + portada.getKey());
                        localCache.set(portada.getKey(), portada, noticiaProperties.getPortadaCacheTimeout());
                    }

                    onResultListener.success(portada);
                }
            });
        }

        @Override
        public void onFailure(final IOException ex, final String callerKey) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onResultListener.failure(new LVIServiceException(ex));
                }
            });
        }
    };

    private PortadaServiceImpl(Context context) {
        noticiaProperties = NoticiasContext.NOTICIA_PROPERTIES;

        if (noticiaProperties.getPortadaCacheEnable()) {
            //localCache = new LVICache(propertiesUtil.getPortadaCacheTimeout());
            localCache = new LVICache(context, Portada.class);
        } else {
            Log.d(LOG_TAG, "CACHE: LVICACHE desactivada para el servicio ...");
        }
    }

    public static PortadaService getInstance(NoticiasContext.AppKeyFramework appKey) throws LVIAppKeyException {
        NoticiasContext.AppKeyFramework.checkKey(appKey);

        if (portadaService == null) {
            portadaService = new PortadaServiceImpl(appKey.getContext());
        }

        return portadaService;
    }

    @Override
    public void obtenerPortada(String bid, String bloques) {
        Portada rPortada = null;

        Portada p = new Portada();
        final String key = p.getKey();
        final int version = p.getVersion();

        if (noticiaProperties.getPortadaCacheEnable()) {
            rPortada = localCache.get(key);
        }

        if (rPortada != null) {
            Log.i(LOG_TAG, "CACHE: Obtengo el objeto de la CACHE ...");
            onResultListener.success(rPortada);
        }

        Log.i(LOG_TAG, noticiaProperties.getObtenerPortadaUrl(bid, bloques));
        HTTP_CLIENT.get(noticiaProperties.getObtenerPortadaUrl(bid, bloques), httpCallback, key);
    }

    @Override
    public void setListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }
}
