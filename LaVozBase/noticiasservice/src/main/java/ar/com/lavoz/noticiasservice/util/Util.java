package ar.com.lavoz.noticiasservice.util;

import android.os.Environment;
import android.support.annotation.NonNull;

import com.google.gson.internal.LinkedTreeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ar.com.lavoz.noticiasservice.model.Nodo;


/**
 * Created by fvalle on 19/10/2015.
 * Utiles
 */
public class Util {

    public static String getImageUrl(Nodo item) {
        String urlBase = item.getStaticUrl();
        String path = "";
        if (item != null) {
            if (item.getImagen() != null && item.getImagen().getClass().isAssignableFrom(ArrayList.class)) {
                ArrayList<String> img = (ArrayList<String>) item.getImagen();
                path = (img != null && img.size() > 0) ? img.get(0) : "";
            } else {
                String img = (String) item.getImagen();
                path = img != null ? img : "";
            }
        }
        return urlBase + path;
    }

    public static String getAutorImageUrl(Nodo item, LinkedTreeMap autor) {
        String urlBase = item.getStaticUrl();
        String path = autor.get("picture") != null ? autor.get("picture").toString() : "";
        return urlBase + path;
    }

    public static String getAutorImageUrl(Nodo item, LinkedHashMap autor) {
        String urlBase = item.getStaticUrl();
        String path = autor.get("picture") != null ? autor.get("picture").toString() : "";
        return urlBase + path;
    }

    @NonNull
    public static String getImageDiskPath(Nodo model) {
        File storagePath = Environment.getExternalStorageDirectory();
        return String.format("%s/com_ar_agrovoz_photo%s%s", storagePath, ((Long) model.getNid()).toString(), ".jpg");
    }

}
