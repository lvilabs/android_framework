package ar.com.lavoz.noticiasservice.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fvalle on 13/10/2015.
 */
public class ApiConfig implements Serializable {

    public int sid;

    public String dominio;

    @SerializedName("static")
    public String static_url;

    public String db;

    public String publicador;

    public String origen;

    public String nombre;

    public String css;

    public String js;

    public String img;

    public String token;

    public String twitter;

    public String facebook_app_id;

    public String facebook_admins;

    public String color;

    public String tags_duros;

    public String uid_admin;

    public String tags_class;
}
