package ar.com.lavoz.noticiasservice.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.com.lavoz.framework.model.Cacheable;

/**
 * Created by fvalle on 22/10/2015.
 * Representa una portadilla
 */
public class Portadilla extends Cacheable implements Serializable {

    public static final int VERSION = 2;

    private String key;
    private int version;
    private Date timestamp;
    private String titulo;
    private String tid;
    private int limit;
    private int page;
    private String format;
    private ApiConfig api;

    private List<Nodo> listado;

    public Portadilla() {
    }

    public Portadilla(String tid){
        key = "portadilla_" + tid;
    }

    @Override
    public String getKey() {
        if(key == null)
            key = "portadilla_" + getTid();
        return key;
    }

    @Override
    public String getKey(String subseccion) {
        if(key == null)
            key = "portadilla_" + subseccion ;
        return key;
    }

    @Override
    public int getVersion() {
        return VERSION;
    }

    public List<Nodo> getNodos() {

        if(listado != null) {
            int i = 0;
            for (Nodo item : listado) {
                item.setStaticUrl(this.getApi().static_url);
                if(i == 0)
                    item.setPrimerItem(true);
                else
                    item.setPrimerItem(false);
                i++;
            }
        }

        List<Nodo> resultado = new ArrayList<>();
        if(listado != null) {
            resultado.addAll(listado);
            resultado.add(new Nodo(-1));
        }
        return resultado;
    }

    public void setListado(List<Nodo> listado) {
        this.listado = listado;
    }

    @Override
    public Date getTimestampCache() {
        return timestamp;
    }

    @Override
    public void setTimestampCache(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public ApiConfig getApi() {
        return api;
    }

    public void setApi(ApiConfig api) {
        this.api = api;
    }
}
