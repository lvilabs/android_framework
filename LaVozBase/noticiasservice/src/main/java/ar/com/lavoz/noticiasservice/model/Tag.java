package ar.com.lavoz.noticiasservice.model;

import java.io.Serializable;

/**
 * Created by fvalle on 13/10/2015.
 */
public class Tag implements Serializable {
    private String name;
    private Long tid;

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }
}
