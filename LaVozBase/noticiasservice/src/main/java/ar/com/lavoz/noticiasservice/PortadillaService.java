package ar.com.lavoz.noticiasservice;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.noticiasservice.model.Portadilla;

/**
 * Created by Dani on 2/16/2016.
 *
 */
public interface PortadillaService {
    /***
     * @param tid
     */
    void obtenerPortadilla(String tid);

    /***
     *
     */
    void obtenerVideos();

    /***
     *
     * @param tid
     * @param page
     * @param limit
     */
    void obtenerPortadillaPagina(String tid, int limit, int page);

    /**
     *
     * @param onResultListener
     */
    void setListener(PortadillaService.OnResultListener onResultListener);

    /***
     *
     */
    interface OnResultListener {
        void success(Portadilla unPortadilla);
        void successPage(Portadilla unPortadilla);

        void failure(LVIServiceException lviEx);
    }

}
