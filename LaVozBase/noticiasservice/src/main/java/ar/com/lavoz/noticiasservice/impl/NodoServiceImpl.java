package ar.com.lavoz.noticiasservice.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;

import ar.com.lavoz.framework.cache.LVICache;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.rest.HttpClient;
import ar.com.lavoz.framework.service.impl.BaseService;
import ar.com.lavoz.framework.util.JsonUtil;
import ar.com.lavoz.noticiasservice.NodoService;
import ar.com.lavoz.noticiasservice.context.NoticiasContext;
import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.noticiasservice.util.NoticiaProperties;

/**
 * Created by Daniel Sandoval on 2/2/2016.
 */
public class NodoServiceImpl extends BaseService implements NodoService {

    static NodoService nodoService;
    static OnResultListener onResultListener;
    NoticiaProperties noticiaProperties;
    LVICache<Nodo> localCache;
    HttpClient.IHttpResult httpCallback = new HttpClient.IHttpResult() {
        @Override
        public void onResponse(final JSONObject result) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.i(LOG_TAG, "RESPONSE::::::" + result.toString());
                    Nodo nodo = new JsonUtil<Nodo>().fromJson(result.toString(), Nodo.class);

                    if (noticiaProperties.getNodoCacheEnable()) {
                        Log.i(LOG_TAG, "SET CACHE .... key:: " + nodo.getKey());
                        localCache.set(nodo.getKey(), nodo, noticiaProperties.getNodoCacheTimeout());
                    }

                    onResultListener.success(nodo);
                }
            });
        }

        @Override
        public void onFailure(final IOException ex, final String callerKey) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onResultListener.failure(new LVIServiceException(ex));
                }
            });
        }
    };

    private NodoServiceImpl(Context context) {
        noticiaProperties = NoticiasContext.NOTICIA_PROPERTIES;

        if (noticiaProperties.getNodoCacheEnable()) {
            localCache = new LVICache(context, Nodo.class);
        } else {
            Log.d(LOG_TAG, "CACHE: LVICACHE desactivada para el servicio ...");
        }
    }

    @Override
    public void setListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    public static NodoService getInstance(NoticiasContext.AppKeyFramework appKey) throws LVIAppKeyException {
        NoticiasContext.AppKeyFramework.checkKey(appKey);

        if (nodoService == null) {
            nodoService = new NodoServiceImpl(appKey.getContext());
        }

        return nodoService;
    }

    public void obtenerNodoWithFailure(String nid) {
        this.obtenerNodo(nid, true);
    }

    public void obtenerNodo(String nid) {
        this.obtenerNodo(nid, false);
    }

    public void obtenerNodo(String nid, boolean generateHttpFailure) {
        Nodo cNodo = null;

        if (noticiaProperties.getNodoCacheEnable() && !generateHttpFailure) {
            cNodo = localCache.get(nid);
        }

        if (cNodo != null) {
            Log.i(LOG_TAG, "CACHE: Obtengo el objeto de la CACHE ...");
            onResultListener.success(cNodo);
        }

        Log.i(LOG_TAG, noticiaProperties.getObtenerNodoUrl(nid));
        HTTP_CLIENT.get(noticiaProperties.getObtenerNodoUrl(nid), httpCallback, nid, generateHttpFailure);
    }

}
