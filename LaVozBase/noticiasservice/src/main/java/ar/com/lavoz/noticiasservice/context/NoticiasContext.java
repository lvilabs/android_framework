package ar.com.lavoz.noticiasservice.context;

import android.content.Context;
import android.util.Log;

import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.framework.util.PropertiesUtil;
import ar.com.lavoz.noticiasservice.MasVistosService;
import ar.com.lavoz.noticiasservice.NodoService;
import ar.com.lavoz.noticiasservice.PortadaService;
import ar.com.lavoz.noticiasservice.PortadillaService;
import ar.com.lavoz.noticiasservice.impl.MasVistosServiceImpl;
import ar.com.lavoz.noticiasservice.impl.NodoServiceImpl;
import ar.com.lavoz.noticiasservice.impl.PortadaServiceImpl;
import ar.com.lavoz.noticiasservice.impl.PortadillaServiceImpl;
import ar.com.lavoz.noticiasservice.util.NoticiaProperties;

/**
 * Punto de acceso a LVI Framework
 */
public class NoticiasContext {
    private AppKeyFramework appKeyFramework;
    public static NoticiaProperties NOTICIA_PROPERTIES;
    public static final String LOG_TAG = "||(LVI) FRAMEWORK:";

    /***
     * @param context
     */
    public NoticiasContext(Context context) throws LVIException {
        Log.d(LOG_TAG, "Creando el NoticiasContext ...");

        if (context == null) {
            Log.e(LOG_TAG, "Objeto [context] es nulo");
            throw new LVIException("Error inicializando el framework, el objeto [context] es nulo.");
        }

        this.appKeyFramework = new AppKeyFramework(context);

        Log.d(LOG_TAG, "Instanciando el archivo de propiedades ...");
        ar.com.lavoz.noticiasservice.context.NoticiasContext.NOTICIA_PROPERTIES = NoticiaProperties.getInstance(context);
    }

    /***
     * @param onResultListener
     * @return
     */
    public NodoService getNodoService(NodoService.OnResultListener onResultListener) {
        NodoService nodoService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio NodoService ...");
            nodoService = NodoServiceImpl.getInstance(appKeyFramework);//--
            nodoService.setListener(onResultListener);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio NodoService.");
        }

        return nodoService;
    }

    /***
     * @param onResultListener
     * @return
     */
    public PortadaService getPortadaService(PortadaService.OnResultListener onResultListener) {
        PortadaService portadaService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio PortadaService ...");
            portadaService = PortadaServiceImpl.getInstance(appKeyFramework);//--
            portadaService.setListener(onResultListener);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio PortadaService.");
        }

        return portadaService;
    }

    /***
     * @param onResultListener
     * @return
     */
    public MasVistosService getMasVistosService(MasVistosService.OnResultListener onResultListener) {
        MasVistosService masVistosService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio PortadaService ...");
            masVistosService = MasVistosServiceImpl.getInstance(appKeyFramework);//--
            masVistosService.setListener(onResultListener);
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio PortadaService.");
        }

        return masVistosService;
    }

    /***
     * @param onResultListener
     * @return
     */
    public PortadillaService getPortadillaService() {
        PortadillaService portadillaService = null;

        try {
            Log.d(LOG_TAG, "Instanciando el servicio PortadillaService ...");
            portadillaService = PortadillaServiceImpl.getInstance(appKeyFramework, null);//--
        } catch (LVIException lviEx) {
            Log.e(LOG_TAG, "Error instanciando el servicio PortadillaService.");
        }

        return portadillaService;
    }

    /***
     * Está clase sirve como llave para evitar accesos a los servicios
     * desde fuera del <b>NoticiasContext</b>.
     */
    public static class AppKeyFramework {
        private Context context;

        private AppKeyFramework(Context context) {
            this.context = context;
        }

        public Context getContext() {
            return context;
        }

        /***
         * @param aKey
         * @throws LVIAppKeyException
         */
        public static void checkKey(AppKeyFramework aKey) throws LVIAppKeyException {
            if (aKey == null) {
                throw new LVIAppKeyException("Se deben instanciar los servicios desde NoticiasContext");
            }
        }
    }

}
