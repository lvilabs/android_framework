package ar.com.lavoz.noticiasservice.model;

import java.io.Serializable;

/**
 * Created by fvalle on 13/10/2015.
 */
public class Video implements Serializable {

    private String provider;
    private String id;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
