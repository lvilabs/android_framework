package ar.com.lavoz.noticiasservice.util;

import android.content.Context;

import java.text.MessageFormat;

import ar.com.lavoz.framework.util.GeneralUtil;
import ar.com.lavoz.framework.util.PropertiesBase;
import ar.com.lavoz.framework.util.PropertiesUtil;

/**
 * Created by Dani on 3/22/2016.
 */
public class NoticiaProperties extends PropertiesBase {
    private static final String FILE_NAME = "noticias.properties";

    public static final String SRV_NODO_CACHE_ENABLE = "service.nodo.cache.enable";
    public static final String SRV_NODO_CACHE_TIMEOUT = "service.nodo.cache.timeout";
    public static final String SRV_NODO_OBTENER_NODO_URL_KEY = "service.nodo.obtenerNodo.url";

    public static final String SRV_PORTADA_CACHE_ENABLE = "service.portada.cache.enable";
    public static final String SRV_PORTADA_CACHE_TIMEOUT = "service.portada.cache.timeout";
    public static final String SRV_PORTADA_OBTENER_PORTADA_URL_KEY = "service.portada.obtenerPortada.url";

    public static final String SRV_MASVISTOS_CACHE_ENABLE = "service.masvistos.cache.enable";
    public static final String SRV_MASVISTOS_CACHE_TIMEOUT = "service.masvistos.cache.timeout";
    public static final String SRV_MASVISTOS_OBTENER_MASVISTOS_URL_KEY = "service.masvistos.obtenerMasVistos.url";

    public static final String SRV_PORTADILLA_CACHE_ENABLE = "service.portadilla.cache.enable";
    public static final String SRV_PORTADILLA_CACHE_TIMEOUT = "service.portadilla.cache.timeout";
    public static final String SRV_PORTADILLA_OBTENER_PORTADILLA_URL_KEY = "service.portadilla.obtenerPortadilla.url";
    public static final String SRV_PORTADILLA_OBTENER_PORTADILLAPAGINA_URL_KEY = "service.portadilla.obtenerPortadillaPagina.url";
    public static final String SRV_PORTADILLA_OBTENER_VIDEOS_URL_KEY = "service.portadilla.obtenerVideos.url";

    private static NoticiaProperties noticiaProperties;
    private static PropertiesUtil propertiesUtil;

    private NoticiaProperties(Context context) {
        loadProperties(FILE_NAME, context);
    }

    public static NoticiaProperties getInstance(Context context) {
        if (noticiaProperties == null) {
            noticiaProperties = new NoticiaProperties(context);
            propertiesUtil = PropertiesUtil.getInstance(context);
        }

        return noticiaProperties;
    }

    public String getObtenerNodoUrl(String nid) {
        return MessageFormat.format(get(SRV_NODO_OBTENER_NODO_URL_KEY), propertiesUtil.getGeneralUrl(), nid);
    }

    public int getNodoCacheTimeout() {
        String opString = get(SRV_NODO_CACHE_TIMEOUT);
        return GeneralUtil.resolveSimpleOperation(opString);
    }

    public boolean getNodoCacheEnable() {
        return Boolean.parseBoolean(get(SRV_NODO_CACHE_ENABLE));
    }

    public String getObtenerPortadaUrl(String bid, String nodos) {
        return MessageFormat.format(get(SRV_PORTADA_OBTENER_PORTADA_URL_KEY), propertiesUtil.getGeneralUrl(), bid, nodos);
    }

    public boolean getPortadaCacheEnable() {
        return Boolean.parseBoolean(get(SRV_PORTADA_CACHE_ENABLE));
    }

    public boolean getMasVistosCacheEnable() {
        return Boolean.parseBoolean(get(SRV_MASVISTOS_CACHE_ENABLE));
    }

    public int getMasVistosCacheTimeout() {
        String opString = get(SRV_MASVISTOS_CACHE_TIMEOUT);
        return GeneralUtil.resolveSimpleOperation(opString);
    }

    public String getObtenerMasVistosUrl() {
        return MessageFormat.format(get(SRV_MASVISTOS_OBTENER_MASVISTOS_URL_KEY), propertiesUtil.getGeneralUrl());
    }

    public int getPortadaCacheTimeout() {
        String opString = get(SRV_PORTADA_CACHE_TIMEOUT);
        return GeneralUtil.resolveSimpleOperation(opString);
    }

    public String getObtenerPortadillaUrl(String tid) {
        return MessageFormat.format(get(SRV_PORTADILLA_OBTENER_PORTADILLA_URL_KEY), propertiesUtil.getGeneralUrl(), tid);
    }

    public String getObtenerVideosUrl() {
        return MessageFormat.format(get(SRV_PORTADILLA_OBTENER_VIDEOS_URL_KEY), propertiesUtil.getGeneralUrl());
    }

    public String getObtenerPortadillaPaginaUrl(String tid, int limit, int page) {
        return MessageFormat.format(get(SRV_PORTADILLA_OBTENER_PORTADILLAPAGINA_URL_KEY), propertiesUtil.getGeneralUrl(), tid, limit, page);
    }

    public boolean getPortadillaCacheEnable() {
        return Boolean.parseBoolean(get(SRV_PORTADILLA_CACHE_ENABLE));
    }

    public int getPortadillaCacheTimeout() {
        String opString = get(SRV_PORTADILLA_CACHE_TIMEOUT);
        return GeneralUtil.resolveSimpleOperation(opString);
    }

}
