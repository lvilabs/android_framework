package ar.com.lavoz.noticiasservice.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;

import ar.com.lavoz.framework.cache.LVICache;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.rest.HttpClient;
import ar.com.lavoz.framework.service.impl.BaseService;
import ar.com.lavoz.framework.util.JsonUtil;
import ar.com.lavoz.noticiasservice.MasVistosService;
import ar.com.lavoz.noticiasservice.context.NoticiasContext;
import ar.com.lavoz.noticiasservice.model.Portadilla;
import ar.com.lavoz.noticiasservice.util.NoticiaProperties;

/**
 * Created by fvalle on 16/02/2016.
 * Implementación del servicio que obtiene los datos del widget de notas mas vistas
 */
public class MasVistosServiceImpl extends BaseService implements MasVistosService {

    static MasVistosService masVistosService;
    static MasVistosService.OnResultListener onResultListener;
    LVICache<Portadilla> localCache;
    NoticiaProperties noticiaProperties;
    HttpClient.IHttpResult httpCallback = new HttpClient.IHttpResult() {

        @Override
        public void onResponse(final JSONObject result) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.i(LOG_TAG, "RESPONSE::::::" + result.toString());
                    Portadilla portadilla = new JsonUtil<Portadilla>().fromJson(result.toString(), Portadilla.class);

                    if (noticiaProperties.getMasVistosCacheEnable()) {
                        Log.i(LOG_TAG, "SET CACHE .... key:: " + portadilla.getKey());
                        localCache.set(portadilla.getKey(), portadilla, noticiaProperties.getMasVistosCacheTimeout());
                    }

                    onResultListener.success(portadilla);
                }
            });
        }

        @Override
        public void onFailure(final IOException ex, final String callerKey) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onResultListener.failure(new LVIServiceException(ex));
                }
            });
        }
    };

    public MasVistosServiceImpl(Context context) {
        noticiaProperties = NoticiasContext.NOTICIA_PROPERTIES;

        if (noticiaProperties.getMasVistosCacheEnable()) {
            localCache = new LVICache(context, Portadilla.class);
        } else {
            Log.d(LOG_TAG, "CACHE: LVICACHE desactivada para el servicio ...");
        }
    }

    public static MasVistosService getInstance(NoticiasContext.AppKeyFramework appKey) throws LVIAppKeyException {
        NoticiasContext.AppKeyFramework.checkKey(appKey);

        if (masVistosService == null) {
            masVistosService = new MasVistosServiceImpl(appKey.getContext());
        }
        return masVistosService;
    }

    @Override
    public void setListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    @Override
    public void obtenerMasVistos() {
        Portadilla rPortadilla = null;
        Portadilla p = new Portadilla("null");
        final String key = p.getKey();

        if (noticiaProperties.getNodoCacheEnable()) {
            rPortadilla = localCache.get(key);
        }

        if (rPortadilla != null) {
            Log.i(LOG_TAG, "CACHE: Obtengo el objeto de la CACHE ...");
            onResultListener.success(rPortadilla);
        }

        Log.i(LOG_TAG, noticiaProperties.getObtenerMasVistosUrl());
        HTTP_CLIENT.get(noticiaProperties.getObtenerMasVistosUrl(), httpCallback);
    }
}
