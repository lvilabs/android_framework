package ar.com.lavoz.noticiasservice.model;


import android.text.Html;

import com.google.gson.Gson;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ar.com.lavoz.framework.model.Cacheable;
import ar.com.lavoz.framework.model.KalturaVideo;
import ar.com.lavoz.framework.util.GeneralUtil;

/**
 * Created by fvalle on 13/10/2015.
 * Clase que representa una Nota
 */
public class Nodo extends Cacheable implements Serializable {

    public Nodo() {

    }

    public Nodo(int bloque) {
        setBloque(bloque);
    }

    private static final String KEY = "nid";

    private long nid;

    private ApiConfig api;

    private String titulo;

    private String bajada;

    private String body;

    private Object imagen;

    private String[] epigrafes;

    private Tag tag_duro;

    private Tag[] tags_libres;

    private String[] agenda;

    private String[] blog;

    private String[] sponsor;

    private long timestamp;

    private LinkedHashMap autor;

    private String sitio;

    private String path;

    private int tiene_video;

    private String clase;

    private Video videos;

    private Date timeStampNota;

    private String[] videos_kaltura;

    public long getNid() {
        return nid;
    }

    public void setNid(long nid) {
        this.nid = nid;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getBajada() {
        return bajada;
    }

    public void setBajada(String bajada) {
        this.bajada = bajada;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Object getImagen() {
        return imagen;
    }

    public void setImagen(Object imagen) {
        this.imagen = imagen;
    }

    public String[] getEpigrafes() {
        return epigrafes;
    }

    public void setEpigrafes(String[] epigrafes) {
        this.epigrafes = epigrafes;
    }

    public Tag getTag_duro() {
        return tag_duro;
    }

    public void setTag_duro(Tag tag_duro) {
        this.tag_duro = tag_duro;
    }

    public Tag[] getTags_libres() {
        return tags_libres;
    }

    public void setTags_libres(Tag[] tags_libres) {
        this.tags_libres = tags_libres;
    }

    public String[] getAgenda() {
        return agenda;
    }

    public void setAgenda(String[] agenda) {
        this.agenda = agenda;
    }

    public String[] getBlog() {
        return blog;
    }

    public void setBlog(String[] blog) {
        this.blog = blog;
    }

    public String[] getSponsor() {
        return sponsor;
    }

    public void setSponsor(String[] sponsor) {
        this.sponsor = sponsor;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public LinkedHashMap getAutor() {
        return autor;
    }

    public void setAutor(LinkedHashMap autor) {
        this.autor = autor;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getTiene_video() {
        return tiene_video;
    }

    public void setTiene_video(int tiene_video) {
        this.tiene_video = tiene_video;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Video getVideos() {
        return videos;
    }

    public void setVideos(Video videos) {
        this.videos = videos;
    }

    public String[] getVideos_kaltura() {
        return videos_kaltura;
    }

    public void setVideos_kaltura(String[] videos_kaltura) {
        this.videos_kaltura = videos_kaltura;
    }

    private String staticUrl;

    public String getStaticUrl() {
        if (staticUrl == null) {
            if (getApi() != null) {
                return getApi().static_url;
            } else {
                return "";
            }
        } else {
            return staticUrl;
        }
    }

    public void setStaticUrl(String staticUrl) {
        this.staticUrl = staticUrl;
    }

    private int bloque;

    public int getBloque() {
        return bloque;
    }

    public void setBloque(int bloque) {
        this.bloque = bloque;
    }

    private Boolean primerItem;

    public Boolean getPrimerItem() {
        return primerItem;
    }

    public void setPrimerItem(Boolean primerItem) {
        this.primerItem = primerItem;
    }

    public String getBajadaTexto() {
        return bajada != null ? Html.fromHtml(bajada).toString() : "";
    }

    public String getBodyTexto() {
        return body != null ? Html.fromHtml(body).toString() : "";
    }

    public String getBodyWithCss() {
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html><html lang=\"es\"><head>");
        //head content
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/fonts.css\" />");
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/bootstrap.min.css\" />");
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/font-awesome.min.css\" />");
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/agrovoz.css\" />");
        sb.append("<meta name=\"viewport\" content=\"user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi\" />");
        sb.append("</head><body class=\"home page page-id-12 page-template page-template-page-composer-php unpress_white_skin articulo-full\">");
        sb.append("<article><div class=\"articulo-full\">");
        //body content
        if (body != null)
            sb.append(body.replace("undefined", ""));

        sb.append("</article></div></body></html>");

        return sb.toString();
    }


    public String getVideoWithCss(Boolean fullScreen) {

        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html><html lang=\"en\"><head>");
        //head content
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/fonts.css\" />");
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/bootstrap.min.css\" />");
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/font-awesome.min.css\" />");
        sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/agrovoz.css\" />");
        if (fullScreen) {
            sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/html/css/video-player.css\" />");
        }
        sb.append("<meta name=\"viewport\" content=\"user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi\" />");
        sb.append("</head><body>");
        //body content
        if (videos_kaltura != null) {
            for (String video : videos_kaltura) {
                if (fullScreen) {
                    if (Html.fromHtml(videos_kaltura[0]).toString().length() > 0) {
                        video = video.replace("\"akamaiHD\"", "autoPlay:true,\"akamaiHD\"");
                    } else {
                        video = video.replace("&width=560&height=315", "&width=100%&height=100%");
                        video = video + "&autoplay=true";
                    }
                }
                sb.append(video);
            }
        }
        sb.append("</body></html>");

        return sb.toString();
    }

    public KalturaVideo getKalturaVideo() {

        KalturaVideo result = null;
        String jsonVideo;
        if (videos_kaltura != null && videos_kaltura.length > 0) {
            jsonVideo = Html.fromHtml(videos_kaltura[0]).toString();
            if (jsonVideo.length() > 0) {
                jsonVideo = jsonVideo.replace("kWidget.embed(", "");
                jsonVideo = jsonVideo.replace(");", "");
                result = new Gson().fromJson(jsonVideo, KalturaVideo.class);
            } else {
                result = new KalturaVideo();
                Pattern pattern = Pattern.compile("entry_id=(.+?)&");
                Matcher matcher = pattern.matcher(videos_kaltura[0]);
                if (matcher.find()) {
                    result.setEntry_id(matcher.group(1));
                }

                pattern = Pattern.compile("uiconf_id/(.+?)/");
                matcher = pattern.matcher(videos_kaltura[0]);
                if (matcher.find()) {
                    result.setUiconf_id(Long.parseLong(matcher.group(1)));
                }

                pattern = Pattern.compile("playerId=(.+?)&");
                matcher = pattern.matcher(videos_kaltura[0]);
                if (matcher.find()) {
                    result.setTargetId(matcher.group(1));
                }

            }
        }
        return result;
    }

    public String getKalturaVideoThumbUrl() {
        String result = null;

        KalturaVideo kalturaVideo = getKalturaVideo();
        if (kalturaVideo != null) {
            //http://streaming.vodgc.com/p/116/sp/11600/thumbnail/entry_id/0_bw94kk3i/version/100032/acv/282/width/300
            result = "http://streaming.vodgc.com/p/116/sp/11600/thumbnail/entry_id/" + kalturaVideo.getEntry_id() + "/version/100032/acv/282/width/500";
        }

        return result;
    }

    public String getFecha() {

        Date date = new Date(timestamp * 1000);

        Date target = GeneralUtil.getTodayAtZero();
        String output;
        DateFormat outputFormatter;
        if (target.after(date)) {
            outputFormatter = new SimpleDateFormat("dd/MM/yyyy");
            output = outputFormatter.format(date);
        } else {
            outputFormatter = new SimpleDateFormat("HH:mm");
            output = outputFormatter.format(date);
        }

        return output;
    }

    public ApiConfig getApi() {
        return api;
    }

    public void setApi(ApiConfig api) {
        this.api = api;
    }

    @Override
    public String getKey() {
        return KEY + getNid();
    }

    @Override
    public int getVersion() {
        return 0;
    }

    @Override
    public Date getTimestampCache() {
        return timeStampNota;
    }


    @Override
    public void setTimestampCache(Date timestampNota) {
        this.timeStampNota = timeStampNota;
    }

    @Override
    public String getKey(String subseccion) {
        return null;
    }

    @Override
    public String toString() {
        return "Nodo{" +
                "nid=" + nid +
                ", api=" + api +
                ", titulo='" + titulo + '\'' +
                ", bajada='" + bajada + '\'' +
                ", body='" + body + '\'' +
                ", imagen=" + imagen +
                ", epigrafes=" + Arrays.toString(epigrafes) +
                ", tag_duro=" + tag_duro +
                ", tags_libres=" + Arrays.toString(tags_libres) +
                ", agenda=" + Arrays.toString(agenda) +
                ", blog=" + Arrays.toString(blog) +
                ", sponsor=" + Arrays.toString(sponsor) +
                ", timestamp=" + timestamp +
                ", autor=" + autor +
                ", sitio='" + sitio + '\'' +
                ", path='" + path + '\'' +
                ", tiene_video=" + tiene_video +
                ", clase='" + clase + '\'' +
                ", videos=" + videos +
                ", timeStampNota=" + timeStampNota +
                ", videos_kaltura=" + Arrays.toString(videos_kaltura) +
                ", staticUrl='" + staticUrl + '\'' +
                ", bloque=" + bloque +
                ", primerItem=" + primerItem +
                '}';
    }
}
