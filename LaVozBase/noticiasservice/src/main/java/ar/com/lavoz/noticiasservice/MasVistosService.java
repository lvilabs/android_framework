package ar.com.lavoz.noticiasservice;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.noticiasservice.model.Portadilla;

/**
 * Created by fvalle on 16/02/2016.
 * Servicio que obtiene los datos del widget de notas mas vistas
 */
public interface MasVistosService {

    /**
     * Obtengo las notas mas vistas
     */
    void obtenerMasVistos();

    /***
     *
     */
    interface OnResultListener {
        void success(Portadilla unPortadilla);

        void failure(LVIServiceException lviEx);
    }

    /**
     *
     * @param onResultListener
     */
    void setListener(MasVistosService.OnResultListener onResultListener);
}
