package ar.com.lavoz.noticiasservice;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.noticiasservice.model.Nodo;

/**
 * Created by Dani on 2/4/2016.
 */
public interface NodoService {
    /***
     *
     * @param nid
     */
    void obtenerNodo(String nid);

    /***
     *
     */
    interface OnResultListener {
        void success(Nodo unNodo);

        void failure(LVIServiceException lviEx);
    }

    /**
     *
     * @param onResultListener
     */
    void setListener(NodoService.OnResultListener onResultListener);
}
