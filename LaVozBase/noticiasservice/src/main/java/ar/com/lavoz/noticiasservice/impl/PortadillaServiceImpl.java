package ar.com.lavoz.noticiasservice.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import ar.com.lavoz.framework.cache.LVICache;
import ar.com.lavoz.framework.exception.LVIAppKeyException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.rest.HttpClient;
import ar.com.lavoz.framework.service.impl.BaseService;
import ar.com.lavoz.framework.util.JsonUtil;
import ar.com.lavoz.noticiasservice.PortadillaService;
import ar.com.lavoz.noticiasservice.context.NoticiasContext;
import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.noticiasservice.model.Portadilla;
import ar.com.lavoz.noticiasservice.util.NoticiaProperties;

/**
 * Created by Dani on 2/16/2016.
 */
public class PortadillaServiceImpl extends BaseService implements PortadillaService {

    static final int DEFAULT_PAGE_SIZE = 10;
    static final String VIDEO_TID = "1";
    static final String PARAM_TID_NAME = "TID";
    static final String PARAM_PAGE = "PAGE";
    static PortadillaService portadillaService;
    static PortadillaService.OnResultListener onResultListener;
    LVICache<Portadilla> localCache;
    NoticiaProperties noticiaProperties;
    HttpClient.IHttpResult httpCallback = new HttpClient.IHttpResult() {
        @Override
        public void onResponse(final JSONObject result) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    Log.i(LOG_TAG, "RESPONSE::::::" + result.toString());
                    Portadilla portadilla = new JsonUtil<Portadilla>().fromJson(result.toString(), Portadilla.class);

                    if (params.containsKey(PARAM_TID_NAME)) {
                        portadilla.setTid(params.get(PARAM_TID_NAME));
                    }

                    if (noticiaProperties.getPortadaCacheEnable()) {
                        String key = portadilla.getKey() + "_" + portadilla.getPage();
                        Log.i(LOG_TAG, "SET CACHE .... key:: " + key);
                        localCache.set(key, portadilla, noticiaProperties.getPortadillaCacheTimeout());
                    }
                    if (params.containsKey(PARAM_PAGE)) {
                        onResultListener.successPage(portadilla);
                    } else {
                        onResultListener.success(portadilla);
                    }
                }

            });

        }

        @Override
        public void onFailure(final IOException ex, final String callerKey) {

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    onResultListener.failure(new LVIServiceException(ex));
                }
            });
        }

    };

    private PortadillaServiceImpl(Context context) {
        noticiaProperties = NoticiasContext.NOTICIA_PROPERTIES;

        if (noticiaProperties.getNodoCacheEnable()) {
            localCache = new LVICache(context, Portadilla.class);
        } else {
            Log.d(LOG_TAG, "CACHE: LVICACHE desactivada para el servicio ...");
        }
    }

    public static PortadillaService getInstance(NoticiasContext.AppKeyFramework appKey, PortadillaService.OnResultListener onRstListener) throws LVIAppKeyException {
        NoticiasContext.AppKeyFramework.checkKey(appKey);

        if (portadillaService == null) {
            onResultListener = onRstListener;
            portadillaService = new PortadillaServiceImpl(appKey.getContext());
        }

        return portadillaService;
    }

    private void obtenerPortadillaBase(String tid, Integer limit, Integer page) {
        Portadilla rPortadilla = null;

        Portadilla p = new Portadilla(tid);
        String key = p.getKey() + "_" + ((page != null) ? page : DEFAULT_PAGE_SIZE);
        final int version = p.getVersion();

        if (noticiaProperties.getPortadillaCacheEnable()) {
            Log.i(LOG_TAG, "GET CACHE .... key:: " + key);
            rPortadilla = localCache.get(key);
        }

        if (rPortadilla != null) {
            Log.i(LOG_TAG, "CACHE: Obtengo el objeto de la CACHE ...");

            Log.i(LOG_TAG, " ------------------ OBJECT CACHE -------------------- ");
            List<Nodo> nodos = rPortadilla.getNodos();
            for (Nodo nodo : nodos) {
                Log.i(LOG_TAG, nodo.toString());
            }
            Log.i(LOG_TAG, " ---------------------------------------------------- ");

            if (page == null && limit == null) {
                onResultListener.success(rPortadilla);
            } else {
                onResultListener.successPage(rPortadilla);
            }
        }

        if (!VIDEO_TID.equals(tid)) {
            HttpClient.IHttpResult.params.put(PARAM_TID_NAME, tid);

            if (page == null && limit == null) {
                Log.i(LOG_TAG, noticiaProperties.getObtenerPortadillaUrl(tid));
                HTTP_CLIENT.get(noticiaProperties.getObtenerPortadillaUrl(tid), httpCallback, key);
            } else {
                HttpClient.IHttpResult.params.put(PARAM_PAGE, "TRUE");
                Log.i(LOG_TAG, noticiaProperties.getObtenerPortadillaPaginaUrl(tid, page, limit));
                HTTP_CLIENT.get(noticiaProperties.getObtenerPortadillaPaginaUrl(tid, page, limit), httpCallback, key);
            }
        } else {
            HttpClient.IHttpResult.params.put(PARAM_TID_NAME, VIDEO_TID);
            Log.i(LOG_TAG, noticiaProperties.getObtenerVideosUrl());
            HTTP_CLIENT.get(noticiaProperties.getObtenerVideosUrl(), httpCallback, key);
        }
    }

    @Override
    public void setListener(OnResultListener onResultListener) {
        PortadillaServiceImpl.onResultListener = onResultListener;
    }

    @Override
    public void obtenerPortadilla(String tid) {
        this.obtenerPortadillaBase(tid, null, null);
    }

    @Override
    public void obtenerVideos() {
        obtenerPortadilla(VIDEO_TID);
    }

    @Override
    public void obtenerPortadillaPagina(String tid, int limit, int page) {
        this.obtenerPortadillaBase(tid, limit, page);
    }

}
