package ar.com.lavoz.noticiasservice;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.noticiasservice.model.Portada;

/**
 * Created by Dani on 2/16/2016.
 * Obtiene los datos para crear la portada de la aplicación
 */
public interface PortadaService {
    /***
     *
     * @param bid Identificador de la portada.
     * @param bloques ids de bloques separados por coma.
     */
    void obtenerPortada(String bid, String bloques);

    /**
     *
     * @param onResultListener
     */
    void setListener(PortadaService.OnResultListener onResultListener);

    /***
     *
     */
    interface OnResultListener {
        void success(Portada unaPortada);

        void failure(LVIServiceException lviEx);
    }

}
