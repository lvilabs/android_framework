package ar.com.lavoz.noticiasservice.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ar.com.lavoz.framework.model.Cacheable;

/**
 * Created by fvalle on 13/10/2015.
 * Representa la portada de la aplicacion
 */
public class Portada extends Cacheable implements Serializable {


    public Portada(){
        key = "portada";
        version = 5;
    }

    private String key;

    private int version;

    private long pid;

    private String titulo;

    private String bids;

    private int limit;

    private int page;

    private String format;

    private ApiConfig api;

    private Nodo[] block_2;

    private Nodo[] block_5;

    private Nodo[] block_6;

    private Nodo[] block_12;

    private Nodo[] block_61;

    private Nodo[] block_96;

    private Nodo[] block_44;


    private Date timestamp;
    
    @Override
    public Date getTimestampCache() {
        return timestamp;
    }

    @Override
    public void setTimestampCache(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getKey(String subseccion) {
        return null;
    }


    public long getPid() {
        return pid;
    }

    
    public void setPid(long pid) {
        this.pid = pid;
    }

    
    public String getTitulo() {
        return titulo;
    }

    
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    
    public String getBids() {
        return bids;
    }

    
    public void setBids(String bids) {
        this.bids = bids;
    }

    
    public int getLimit() {
        return limit;
    }

    
    public void setLimit(int limit) {
        this.limit = limit;
    }

    
    public int getPage() {
        return page;
    }

    
    public void setPage(int page) {
        this.page = page;
    }

    
    public String getFormat() {
        return format;
    }

    
    public void setFormat(String format) {
        this.format = format;
    }

    
    public ApiConfig getApi() {
        return api;
    }

    
    public void setApi(ApiConfig api) {
        this.api = api;
    }

    
    public Nodo[] getBlock_2() {
        return block_2;
    }

    
    public void setBlock_2(Nodo[] block_2) {
        this.block_2 = block_2;
    }

    
    public Nodo[] getBlock_5() {
        return block_5;
    }

    
    public void setBlock_5(Nodo[] block_5) {
        this.block_5 = block_5;
    }

    
    public Nodo[] getBlock_6() {
        return block_6;
    }

    
    public void setBlock_6(Nodo[] block_6) {
        this.block_6 = block_6;
    }

    
    public Nodo[] getBlock_12() {
        return block_12;
    }

    
    public void setBlock_12(Nodo[] block_12) {
        this.block_12 = block_12;
    }

    
    public Nodo[] getBlock_61() {
        return block_61;
    }

    
    public void setBlock_61(Nodo[] block_61) {
        this.block_61 = block_61;
    }

    
    public Nodo[] getBlock_96() {
        return block_96;
    }

    
    public void setBlock_96(Nodo[] block_96) {
        this.block_96 = block_96;
    }

    
    public Nodo[] getBlock_44() {
        return block_44;
    }

    
    public void setBlock_44(Nodo[] block_44) {
        this.block_44 = block_44;
    }


    public List<Nodo> getNodos() {

        List<Nodo> model = new ArrayList<Nodo>();

        if(this.getBlock_2()!= null) {
            for ( Nodo item : this.getBlock_2()) {
                item.setBloque(2);
                item.setStaticUrl(this.getApi().static_url);
                model.add(item);
            }
        }

        //agrego el primer banner
        model.add(new Nodo( -1001 ));

        if(this.getBlock_96()!= null) {
            int i = 0;
            for ( Nodo item : this.getBlock_96()) {
                item.setBloque(96);
                item.setStaticUrl(this.getApi().static_url);
                item.setPrimerItem(i == 0);
                model.add(item);
                i++;
            }
        }

        //agrego el segundo banner
        model.add(new Nodo( -1002 ));

        if(this.getBlock_5()!= null) {
            for ( Nodo item : this.getBlock_5()) {
                item.setBloque(5);
                item.setStaticUrl(this.getApi().static_url);
                model.add(item);
            }
        }
        if(this.getBlock_6()!= null) {
            for ( Nodo item : this.getBlock_6()) {
                item.setBloque(6);
                item.setStaticUrl(this.getApi().static_url);
                model.add(item);
            }
        }
        if(this.getBlock_12()!= null) {
            for ( Nodo item : this.getBlock_12()) {
                item.setBloque(12);
                item.setStaticUrl(this.getApi().static_url);
                model.add(item);
            }
        }
        if(this.getBlock_61()!= null) {
            int i = 0;
            for ( Nodo item : this.getBlock_61()) {
                item.setBloque(61);
                item.setStaticUrl(this.getApi().static_url);
                item.setPrimerItem(i == 0);
                model.add(item);
                i++;
            }
        }

        if(this.getBlock_44()!= null) {
            for ( Nodo item : this.getBlock_44()) {
                item.setBloque(44);
                item.setStaticUrl(this.getApi().static_url);
                model.add(item);
            }
        }

        //agrego el Nodo de agroverdad
        model.add( new Nodo(-1));

        //agrego el Nodo footer
        model.add(new Nodo(-2));

        return model;
    }


    @Override
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
