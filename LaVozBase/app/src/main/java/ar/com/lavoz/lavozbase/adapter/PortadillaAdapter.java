package ar.com.lavoz.lavozbase.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.adapter.viewholder.BasePortadaItemHolder;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemFooterVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemNormalVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemVideoListaVH;

/**
 * Created by fvalle on 14/10/2015.
 * Adaptador de la lista de notas para una portadilla
 */
public class PortadillaAdapter extends RecyclerView.Adapter<BasePortadaItemHolder> {

    private List<Nodo> bloques;

    public PortadillaAdapter(List<Nodo> bloques){
        this.bloques = bloques;
    }

    @Override
    public BasePortadaItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        switch(viewType) {
            case 1: {
                View itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.portada_item_video_list, parent, false);
                return new PortadaItemVideoListaVH(itemView);
            }
            case -1:{
                View itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.portada_item_footer, parent, false);
                return new PortadaItemFooterVH(itemView);
            }
            default: {
                View itemView = LayoutInflater
                        .from(parent.getContext()).inflate(R.layout.portada_item_normal, parent, false);
                    return new PortadaItemNormalVH(itemView);
            }
        }
    }

    @Override
    public void onBindViewHolder(BasePortadaItemHolder holder, int position) {

        holder.setItem(bloques.get(position));
    }

    @Override
    public int getItemCount() {
        return bloques.toArray().length;
    }

    @Override
    public int getItemViewType(int position) {

        int viewType = 0;
        //return bloques.get(position).getTiene_video();
        if(bloques.get(position).getTiene_video() == 1)
            viewType = bloques.get(position).getTiene_video();
        else if(bloques.get(position).getBloque() == -1)
            viewType = bloques.get(position).getBloque();
        return viewType;
    }
}
