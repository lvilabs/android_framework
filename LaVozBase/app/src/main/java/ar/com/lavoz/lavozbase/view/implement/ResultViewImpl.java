package ar.com.lavoz.lavozbase.view.implement;

import android.app.Activity;
import android.os.Bundle;

import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.view.ResultView;

/**
 * Created by desarrollo on 8/3/16.
 *
 */
public class ResultViewImpl extends Activity implements ResultView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
    }
}

