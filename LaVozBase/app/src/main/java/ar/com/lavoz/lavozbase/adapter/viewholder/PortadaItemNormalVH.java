package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.util.Util;

/**
 * Created by fvalle on 14/10/2015.
 * Viewholder del item proncipal de portada
 */
public class PortadaItemNormalVH extends BasePortadaItemHolder {

    private TextView titulo;
    private TextView tagDuro;
    private TextView bajada;
    private ImageView imagen;
    private Boolean tieneImagen;

    public PortadaItemNormalVH(View itemView) {
        super(itemView);
    }

    @Override
    public void onClick(View v) {
        /*
        Intent intent = new Intent(context, NotaView.class );
        intent.putExtra(NotaView.EXTRA_BLOQUE, bloque);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && tieneImagen) {

            try {
                Activity activity = Util.scanForActivity(context);
                View statusBar = activity.findViewById(android.R.id.statusBarBackground);
                View navigationBar = activity.findViewById(android.R.id.navigationBarBackground);
                ActivityOptionsCompat options;

                if(navigationBar != null) {
                    options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            // the context of the activity
                            activity,
                            // For each shared element, add to this method a new Pair item,
                            // which contains the reference of the view we are transitioning *from*,
                            // and the value of the transitionName attribute

                            Pair.create(v.findViewById(R.id.imgImagen), context.getString(R.string.transition_name_nota_foto))
                            , Pair.create(activity.findViewById(R.id.appbar), context.getString(R.string.transition_name_appbar))
                            , Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME)
                            , Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME));
                }else{
                    options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            // the context of the activity
                            activity,
                            // For each shared element, add to this method a new Pair item,
                            // which contains the reference of the view we are transitioning *from*,
                            // and the value of the transitionName attribute

                            Pair.create(v.findViewById(R.id.imgImagen), context.getString(R.string.transition_name_nota_foto))
                            , Pair.create(activity.findViewById(R.id.appbar), context.getString(R.string.transition_name_appbar))
                            , Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME));
                }
                ActivityCompat.startActivity(activity, intent, options.toBundle());
            }catch (Exception e) {
                e.printStackTrace();
                context.startActivity(intent);
            }

        }else{
            context.startActivity(intent);
        }*/

    }

    @Override
    public void setItem(Nodo item){
        super.setItem(item);

        titulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        imagen = (ImageView) itemView.findViewById(R.id.imgImagen);
        tagDuro  = (TextView) itemView.findViewById(R.id.txtTagDuro);
        bajada = (TextView) itemView.findViewById(R.id.txtBajada);

        tagDuro.setText(item.getTag_duro() != null ? item.getTag_duro().getName(): "");

        String imgUrl = Util.getImageUrl(item);
        if(!imgUrl.equals(item.getStaticUrl())) {
            ImageLoader.getInstance().displayImage(imgUrl, imagen);
            imagen.setVisibility(View.VISIBLE);
            bajada.setVisibility(View.GONE);
            tieneImagen = true;
        }else{
            imagen.setVisibility(View.GONE);
            bajada.setText(item.getBajadaTexto());
            bajada.setVisibility(View.VISIBLE);
            tieneImagen = false;
        }

        titulo.setText(item.getTitulo());
    }
}
