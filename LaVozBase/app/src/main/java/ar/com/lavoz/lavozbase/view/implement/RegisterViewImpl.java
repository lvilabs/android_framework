package ar.com.lavoz.lavozbase.view.implement;

import android.os.Bundle;

import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.view.BaseActivity;

/**
 * Created by fvalle on 13/04/2016.
 */
public class RegisterViewImpl extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
    }
}
