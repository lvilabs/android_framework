package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.util.Util;

/**
 * Created by fvalle on 14/10/2015.
 * Portada Item Video View Holder
 */
public class PortadaItemVideoVH extends BasePortadaItemHolder {

    private TextView titulo;
    private ImageView imagen;
    private Context context;
    //private PortadillaService portadillaService;
    private RecyclerView videosSubList;

    public PortadaItemVideoVH(View itemView) {

        super(itemView);
        //portadillaService = new PortadillaService();

    }

    @Override
    public void setItem(Nodo item) {

        super.setItem(item);

        titulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        imagen = (ImageView) itemView.findViewById(R.id.imgImagen);
        videosSubList = (RecyclerView) itemView.findViewById(R.id.videosSubList);
        context = itemView.getContext();

        ImageLoader.getInstance().displayImage(Util.getImageUrl(item), imagen);
        titulo.setText(item.getTitulo());
        //portadillaService.obtenerDatos(context, "1");

    }

    /*public void onEvent(PortadillaService.OnSuccess event) {
        List<Bloque> items = event.getPortadilla().getBloques();
        List<Bloque> subItems = new ArrayList<>();
        if(items != null){
            if(items.size() > 2)
                subItems = items.subList(0, 2);
            else
                subItems = items;
        }

        UltimosVideosAdapter adapter = new UltimosVideosAdapter(subItems);
        videosSubList.setHasFixedSize(false);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        videosSubList.setLayoutManager(llm);
        videosSubList.setAdapter(adapter);

    }*/
}
