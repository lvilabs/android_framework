package ar.com.lavoz.lavozbase.view.implement;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.inject.Inject;

import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.presenter.PortadillaPresenter;
import ar.com.lavoz.lavozbase.view.BaseActivity;
import ar.com.lavoz.lavozbase.view.PortadillaView;

/**
 * Created by fvalle on 14/10/2015.
 * Vista de una portadilla
 */
public class PortadillaViewImpl extends BaseActivity implements PortadillaView {

    public static String EXTRA_TID = "tid";
    public static String EXTRA_TAG = "tag";
    @Inject
    PortadillaPresenter presenter;
    private String tid;
    private RecyclerView recycler;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_portadilla);

            setRecycler((RecyclerView) findViewById(R.id.cardList));
            setSpinner((ProgressBar) findViewById(R.id.progressBar));
            Intent intent = this.getIntent();

            presenter.setView(this);
            presenter.init(false);

            //setupToolbar();
            //setupMenu();

            /*if (intent != null) {

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    setTid(bundle.getString(EXTRA_TID));
                    String tag = bundle.getString(EXTRA_TAG);
                    TextView txtTag = (TextView) findViewById(R.id.txtTag);
                    txtTag.setText(tag);
                    presenter.obtenerDatos();
                }
            }*/

            setTid("10204");
            String tag = "Ganadería";
            TextView txtTag = (TextView) findViewById(R.id.txtTag);
            txtTag.setText(tag);

            presenter.obtenerDatos();
        } catch (Exception e) {
            Log.w("PortadillaView - Create", Log.getStackTraceString(e));
            //irAPortada();
        }
    }

    @Override
    protected void onDestroy() {
        // Unregister
        //presenter.unRegisterEvent();
        super.onDestroy();
    }

    @Override
    public void onRestart() {
        super.onRestart();
        //When BACK BUTTON is pressed, the activity on the stack is restarted
        //Do what you want on the refresh procedure here
        //presenter.obtenerDatos();
    }

    @Override
    public RecyclerView getRecycler() {
        return recycler;
    }

    @Override
    public void setRecycler(RecyclerView recycler) {
        this.recycler = recycler;
    }

    @Override
    public ProgressBar getSpinner() {
        return this.spinner;
    }

    @Override
    public void setSpinner(ProgressBar spinner) {
        this.spinner = spinner;
    }

    @Override
    public Context getContext() {
        return PortadillaViewImpl.this;
    }

    @Override
    public String getTid() {
        return tid;
    }

    @Override
    public void setTid(String tid) {
        this.tid = tid;
    }
}
