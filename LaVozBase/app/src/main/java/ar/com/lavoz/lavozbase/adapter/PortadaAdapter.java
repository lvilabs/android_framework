package ar.com.lavoz.lavozbase.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;
import java.util.List;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.adapter.viewholder.BasePortadaItemHolder;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemAgroverdadVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemBanner1;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemBanner2;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemElDatoVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemFooterVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemNormalVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemOpinionVH;
import ar.com.lavoz.lavozbase.adapter.viewholder.PortadaItemVideoVH;

/**
 * Created by fvalle on 14/10/2015.
 * Adaptador que muestra los items de la portada
 */
public class PortadaAdapter extends RecyclerView.Adapter<BasePortadaItemHolder> {

    private List<Nodo> bloques;

    public PortadaAdapter(List<Nodo> bloques){
        this.bloques = bloques;
    }

    @Override
    public BasePortadaItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layoutId;
        Class viewHolder;

        switch (viewType) {
            case 2:{
                layoutId = R.layout.portada_item_normal;
                viewHolder = PortadaItemNormalVH.class;
                break;
            }
            case 44:{
                layoutId = R.layout.portada_item_video;
                viewHolder = PortadaItemVideoVH.class;
                break;
            }case 96: {
                layoutId = R.layout.portada_item_el_dato;
                viewHolder = PortadaItemElDatoVH.class;
                break;
            }
            case 5:
            case 12:
            case 6: {
                layoutId = R.layout.portada_item_normal;
                viewHolder = PortadaItemNormalVH.class;
                break;
            }
            case 61: {
                layoutId = R.layout.portada_item_opinion;
                viewHolder = PortadaItemOpinionVH.class;
                break;
            }
            case -1:{
                layoutId = R.layout.portada_item_agroverdad;
                viewHolder = PortadaItemAgroverdadVH.class;
                break;
            }
            case -2:{
                layoutId = R.layout.portada_item_footer;
                viewHolder = PortadaItemFooterVH.class;
                break;
            }
            case -1001:{
                layoutId = R.layout.portada_item_banner;
                viewHolder = PortadaItemBanner1.class;
                break;
            }
            case -1002:{
                layoutId = R.layout.portada_item_banner;
                viewHolder = PortadaItemBanner2.class;
                break;
            }
            default: {
                layoutId = R.layout.portada_item_normal;
                viewHolder = PortadaItemNormalVH.class;
            }
        }

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(layoutId, parent, false);

        Constructor<?> ctor;

        try {
            ctor = viewHolder.getConstructor(View.class);
            return (BasePortadaItemHolder)ctor.newInstance(itemView);
        } catch (Exception e) {
            Log.w("Portada", e.getMessage());
        }

        return null;
    }

    @Override
    public void onBindViewHolder(BasePortadaItemHolder holder, int position) {

        holder.setItem(bloques.get(position));
    }

    @Override
    public int getItemCount() {
        return bloques.toArray().length;
    }

    @Override
    public int getItemViewType(int position) {

        return bloques.get(position).getBloque();
    }
}
