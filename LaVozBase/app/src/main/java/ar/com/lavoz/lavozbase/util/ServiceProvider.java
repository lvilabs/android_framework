package ar.com.lavoz.lavozbase.util;

import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.lavozbase.app;
import ar.com.lavoz.noticiasservice.PortadillaService;
import ar.com.lavoz.noticiasservice.context.NoticiasContext;

/**
 * Created by Dani on 3/1/2016.
 *
 */
public class ServiceProvider {
    PortadillaService portadillaService;

    public ServiceProvider() {
        try {
            NoticiasContext appFrameworkContext = new NoticiasContext(app.getAppContext());
            this.portadillaService = appFrameworkContext.getPortadillaService();
        } catch (LVIException e) {
            e.printStackTrace();
        }
    }

    public PortadillaService getPortadillaService() {
        return portadillaService;
    }

}
