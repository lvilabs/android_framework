package ar.com.lavoz.lavozbase.view.implement;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.auth.GoogleLogin;
import ar.com.lavoz.lavozbase.util.Util;

/**
 * Created by fvalle on 06/04/2016.
 *
 */
public class LoginViewImpl extends FragmentActivity {
    private static final String TAG = "LOGIN";
    private TextView mStatusTextView;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        registerButton = (Button)findViewById(R.id.btnRegister);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginViewImpl.this,RegisterViewImpl.class);
                startActivity(intent);
            }
        });

        GoogleLogin.getInstance().init(this);
        GoogleLogin.getInstance().setFailListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {

            }
        });

        mStatusTextView = (TextView)findViewById(R.id.status);
        // Customize sign-in button. The sign-in button can be displayed in
        // multiple sizes and color schemes. It can also be contextually
        // rendered based on the requested scopes. For example. a red button may
        // be displayed when Google+ scopes are requested, but a white button
        // may be displayed when only basic profile is requested. Try adding the
        // Scopes.PLUS_LOGIN scope to the GoogleSignInOptions to see the
        // difference.
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setColorScheme(SignInButton.COLOR_LIGHT);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(GoogleLogin.getInstance().getGso().getScopeArray());
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleLogin.getInstance().signIn();
            }
        });

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        int height = (int) Util.convertDpToPixel(90, LoginViewImpl.this);
        loginButton.setHeight(height);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                updateUI(true);
            }

            @Override
            public void onCancel() {
                // App code
                updateUI(false);
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                updateUI(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GoogleLogin.RC_SIGN_IN) {
            if (!GoogleLogin.getInstance().getmGoogleApiClient().isConnecting()) {
                GoogleLogin.getInstance().getmGoogleApiClient().connect();
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                Log.d(TAG, "handleSignInResult:" + result.isSuccess());
                if (result.isSuccess()) {
                    // Signed in successfully, show authenticated UI.
                    GoogleSignInAccount acct = result.getSignInAccount();
                    if(acct != null) {
                        Log.i(TAG, "Email: " + acct.getEmail() + "Nombre: " + acct.getDisplayName());
                        //mStatusTextView.setText("Email: " + acct.getEmail() + "Nombre: " + acct.getDisplayName());
                        mStatusTextView.setText(getString(R.string.signed_in_fmt));
                    }
                    updateUI(true);
                } else {
                    // Signed out, show unauthenticated UI.
                    updateUI(false);
                }
            }
        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void updateUI(Boolean loged){
        if(loged) {
            Intent mIntent = new Intent(this, PortadillaViewImpl.class);
            startActivity(mIntent);
            this.finish();
        }else {
            mStatusTextView.setText("No se pudo procesar el login");
        }
    }
}
