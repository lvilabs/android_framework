package ar.com.lavoz.lavozbase.presenter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.noticiasservice.model.Portadilla;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.adapter.PortadaAdapter;
import ar.com.lavoz.lavozbase.adapter.PortadillaAdapter;
import ar.com.lavoz.lavozbase.util.ServiceProvider;
import ar.com.lavoz.lavozbase.view.PortadillaView;
import ar.com.lavoz.noticiasservice.PortadillaService;

/**
 * Created by fvalle on 14/10/2015.
 * Presentador de la lista de noticias de una portadilla (seccion)
 */
public class PortadillaPresenter {

    private RecyclerView.Adapter adapter;
    private List<Nodo> model;
    private int nextPage;
    private boolean loadingPage;
    private PortadillaService service;
    private PortadillaView view;

    @Inject
    ServiceProvider serviceProvider;

    public static final int PAGE_SIZE = 10;

    public PortadillaPresenter() {
        this.nextPage = 2;
        model = new ArrayList<>();
    }

    public void init(boolean esPortada) {
        if (esPortada)
            adapter = new PortadaAdapter(model);
        else
            adapter = new PortadillaAdapter(model);

        view.getRecycler().setHasFixedSize(false);
        final LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        view.getRecycler().setLayoutManager(llm);
        view.getRecycler().setAdapter(adapter);

        view.getRecycler().addOnScrollListener(onScrollListener);
        view.getSpinner().setVisibility(View.GONE);
        Log.d("portadilla", "oculto el spinner");

        this.service = serviceProvider.getPortadillaService();
        this.service.setListener(poOnResultListener);

        loadingPage = false;
    }

    public void setView(PortadillaView view) {
        this.view = view;
    }

    PortadillaService.OnResultListener poOnResultListener = new PortadillaService.OnResultListener() {

        @Override
        public void successPage(final Portadilla unPortadilla) {
            if (unPortadilla.getNodos() != null && unPortadilla.getNodos().size() > 0) {
                if (model.size() > 0) {
                    model.remove(model.size() - 1);
                }
                model.addAll(unPortadilla.getNodos());
                adapter.notifyDataSetChanged();
                nextPage++;
            }

            Log.d("portadilla", "oculto el spinner");
            view.getSpinner().setVisibility(View.GONE);
            loadingPage = false;
        }

        @Override
        public void success(final Portadilla unPortadilla) {
            if ((unPortadilla.getTid() == null && view.getTid().equals(view.getContext().getString(R.string.tid_videos))) ||
                    unPortadilla.getTid() != null && unPortadilla.getTid().equals(view.getTid())) {

                List<Nodo> bloques = unPortadilla.getNodos();
                if (model != null && bloques != null) {
                    model.clear();
                    model.addAll(bloques);
                    adapter.notifyDataSetChanged();
                }

                Log.d("portadilla", "oculto el spinner");
                view.getSpinner().setVisibility(View.GONE);
                loadingPage = false;

                /*Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Log.d("portadilla", "oculto el spinner");
                        view.getSpinner().setVisibility(View.GONE);
                        loadingPage = false;
                    }
                }, 200);*/
            }

        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.d("portadilla", "oculto el spinner");
            view.getSpinner().setVisibility(View.GONE);
            loadingPage = false;
        }
    };

    public void obtenerDatos() {
        Log.d("portadilla", "LLAMANDO A OBTENERDATOS().........");
        this.view.getSpinner().setVisibility(View.VISIBLE);
        Log.d("portadilla", "muestro el spinner");
        this.service.obtenerPortadilla(view.getTid());
    }

    private void cargarPagina() {
        if (!loadingPage) {
            loadingPage = true;
            Log.d("portadilla", "LLAMANDO A CARGAR PAGINA().........");
            this.view.getSpinner().setVisibility(View.VISIBLE);
            Log.d("portadilla", "muestro el spinner");
            this.service.obtenerPortadillaPagina(view.getTid(), PAGE_SIZE, PAGE_SIZE * nextPage);
        }
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (!recyclerView.canScrollVertically(1)) {
                Log.d("PORTADILLA", "scroll........");
                //scrolled to bottom
                //cargo una nueva página
                cargarPagina();
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    };

}
