package ar.com.lavoz.lavozbase.auth;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by fvalle on 06/04/2016.
 *
 */
public class GoogleLogin {

    private GoogleSignInOptions gso;
    private GoogleApiClient.OnConnectionFailedListener mFailListener;
    private static GoogleLogin instance;
    private FragmentActivity mFragment;
    private GoogleApiClient mGoogleApiClient;

    /* Request code used to invoke sign in user interactions. */
    public static final int RC_SIGN_IN = 0;

    public static GoogleLogin getInstance(){
        if(instance == null) {
            instance = new GoogleLogin();
        }
        return instance;
    }

    public GoogleLogin init(FragmentActivity fragment){
        mFragment = fragment;
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(mFragment)
                .enableAutoManage(mFragment /* FragmentActivity */, mFailListener /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        return instance;
    }

    public GoogleApiClient getmGoogleApiClient(){
        return mGoogleApiClient;
    }

    public GoogleSignInOptions getGso(){
        return gso;
    }

    public void setFailListener(GoogleApiClient.OnConnectionFailedListener failListener){
        mFailListener = failListener;
    }

    public void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        mFragment.startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
