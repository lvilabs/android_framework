package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import ar.com.lavoz.noticiasservice.model.Nodo;

/**
 * Created by fvalle on 16/10/2015.
 * Clase base para todo los viewholders que muestran los items de portada
 */
public abstract class BasePortadaItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected Nodo bloque;
    protected Context context;

    public BasePortadaItemHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void setItem(Nodo item){
        bloque = item;
    }

    @Override
    public void onClick(View v) {
        mostrarNota();
    }

    protected void mostrarNota() {
        //Intent intent = new Intent(context, NotaView.class );
        //intent.putExtra(NotaView.EXTRA_BLOQUE, bloque );
        //context.startActivity(intent);
    }

}
