package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.util.Util;

/**
 * Created by fvalle on 05/11/2015.
 * Viewholder de un item de la portada que muestra el bloque Agroverdad
 */
public class PortadaItemFooterVH extends BasePortadaItemHolder {

    public PortadaItemFooterVH(View itemView){
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Util.openUrl("http://www.lavoz.com.ar/lvilab/", context);
            }
        });
    }

    @Override
    public void setItem(Nodo item) {

        super.setItem(item);
    }

}
