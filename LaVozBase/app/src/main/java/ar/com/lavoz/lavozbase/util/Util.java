package ar.com.lavoz.lavozbase.util;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;

import com.facebook.AccessToken;
import com.google.gson.internal.LinkedTreeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;

/**
 * Created by fvalle on 19/10/2015.
 * Utiles
 */
public class Util {

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static String getImageUrl(Nodo item) {
        String urlBase = item.getStaticUrl();
        String path = "";
        if (item != null) {
            if (item.getImagen() != null && item.getImagen().getClass().isAssignableFrom(ArrayList.class)) {
                ArrayList<String> img = (ArrayList<String>) item.getImagen();
                path = (img != null && img.size() > 0) ? img.get(0) : "";
            } else {
                String img = (String) item.getImagen();
                path = img != null ? img : "";
            }
        }
        return urlBase + path;
    }

    public static String getAutorImageUrl(Nodo item, LinkedTreeMap autor) {
        String urlBase = item.getStaticUrl();
        String path = autor.get("picture") != null ? autor.get("picture").toString() : "";
        return urlBase + path;
    }

    public static String getAutorImageUrl(Nodo item, LinkedHashMap autor) {
        String urlBase = item.getStaticUrl();
        String path = autor.get("picture") != null ? autor.get("picture").toString() : "";
        return urlBase + path;
    }

    public static String getTidPorTag(String tagNombre) {
        String tid = "";

        switch (tagNombre) {
            case "Actualidad":
                tid = "10206";
                break;
            case "Agricultura":
                tid = "10203";
                break;
            case "Ganadería":
                tid = "10204";
                break;
            case "Entrevistas":
                tid = "264053";
                break;
            case "Tecnología":
                tid = "10202";
                break;
            case "Clima":
                tid = "10205";
                break;
        }

        return tid;
    }

    public static Date getTodayAtZero() {
        long millisInDay = 60 * 60 * 24 * 1000;
        long currentTime = new Date().getTime();
        long dateOnly = (currentTime / millisInDay) * millisInDay;
        return new Date(dateOnly);
    }

    public static AccessToken getFbAppToken(Context context) {

        return new AccessToken(
                context.getString(R.string.facebook_app_token),
                context.getString(R.string.facebook_app_id),
                context.getString(R.string.facebook_user_id),
                null,
                null,
                null,
                null,
                null);
    }

    public static Activity scanForActivity(Context cont) {
        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;
        else if (cont instanceof ContextWrapper)
            return scanForActivity(((ContextWrapper) cont).getBaseContext());

        return null;
    }

    public static void openUrl(String url, Context context) {
        final Intent intent = new Intent(Intent.ACTION_VIEW)
                .setData(Uri.parse(url));
        context.startActivity(intent);
    }

    @SuppressWarnings("deprecation")
    public static final Drawable getDrawable(Context context, int id) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getDrawable(id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    @SuppressWarnings("deprecation")
    public static final int getColor(Context context, int id) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    @NonNull
    public static String getImageDiskPath(Nodo model) {
        File storagePath = Environment.getExternalStorageDirectory();
        return String.format("%s/com_ar_agrovoz_photo%s%s", storagePath, ((Long) model.getNid()).toString(), ".jpg");
    }

}
