package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.util.Util;

/**
 * Created by fvalle on 14/10/2015.
 * Viewholder que muestra un item de la portadilla de videos
 */
public class PortadaItemVideoListaVH extends BasePortadaItemHolder {

    private TextView titulo;
    private ImageView imagen;
    private TextView tagDuro;

    public PortadaItemVideoListaVH(View itemView) {

        super(itemView);
    }

    @Override
    public void setItem(Nodo item){

        super.setItem(item);

        titulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        imagen = (ImageView) itemView.findViewById(R.id.imgImagen);
        tagDuro = (TextView) itemView.findViewById(R.id.txtTagDuro);
        context = itemView.getContext();

        ImageLoader.getInstance().displayImage(Util.getImageUrl(item), imagen);
        titulo.setText(item.getTitulo());
        if(tagDuro != null)
            tagDuro.setText(item.getTag_duro() != null ? item.getTag_duro().getName() : "");
    }
}
