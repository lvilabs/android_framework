package ar.com.lavoz.lavozbase;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.service.GcmService;
import ar.com.lavoz.lavozbase.notifications.Notification;
/*
import org.apache.commons.lang3.ObjectUtils;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.service.GcmService;
import ar.com.lavoz.lavozbase.notifications.Notification;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;*/

/**
 * Created by fvalle on 14/10/2015.
 * Maneja la configuración a nivel de la aplicación
 */
public class app extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "uirRGDcbsLoahc7K6aimOGIeK";
    private static final String TWITTER_SECRET = "LcKNEo15dfwoVDqQUCIBySflJXcgj94F8FKEdaREkDYc19VM3a";
    private static Context mContext;

    public static Context getAppContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        //initCalligraphy();
        initImageLoader(getApplicationContext());
        initNotificationsService();
        initFacebook();
    }

    private void initFacebook(){
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private void initNotificationsService(){
        try {
            AppFrameworkContext appFrameworkContext = new AppFrameworkContext(app.this);
            GcmService gcmService = appFrameworkContext.getGcmService();
            gcmService.addTopic("global");
            gcmService.addListener(onResultListener);
            gcmService.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d("GCM Notification", "on Broadcast Receive OK");
                }
            });
            gcmService.startService();

        } catch (LVIException e) {
            e.printStackTrace();
        }

    }

    GcmService.OnResultListener  onResultListener = new GcmService.OnResultListener() {
        @Override
        public void successByTopic(Bundle data) {
            Notification.getInstance().showNotification(app.this, 0, "Topico Global", data.getString("message"));
        }

        @Override
        public void successByDeviceID(Bundle data) {
            Notification.getInstance().showNotification(app.this, 0, "Device Id", data.getString("message"));
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.w("GCM Notification", lviEx.getMessage());
        }
    };

    /*
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }*/

    /*
    private void initCalligraphy() {
        //seteo la fuente por defecto para toda la aplicación
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Lato-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }*/

    public void initImageLoader(Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.default_no_image)
                .showImageOnFail(R.drawable.default_no_image)
                .showImageOnLoading(R.drawable.default_no_image)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.defaultDisplayImageOptions(options);
        ImageLoader.getInstance().init(config.build());

    }
}
