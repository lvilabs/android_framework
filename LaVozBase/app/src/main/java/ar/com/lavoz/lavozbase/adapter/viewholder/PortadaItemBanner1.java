package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;

import ar.com.lavoz.noticiasservice.model.Nodo;

/**
 * Created by fvalle on 06/11/2015.
 * ViewHolder Para el primer banner de portada
 */
public class PortadaItemBanner1 extends BasePortadaItemBannerVH {

    private static Boolean esNuevaVista;

    public PortadaItemBanner1(View itemView) {
        super(itemView);
        esNuevaVista = true;
    }

    @Override
    public void setItem(Nodo item) {
        if(esNuevaVista) {
            super.setItem(item);
            esNuevaVista = false;
        }

    }
}
