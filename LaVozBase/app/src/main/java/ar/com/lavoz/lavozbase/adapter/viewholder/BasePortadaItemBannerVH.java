package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;
import android.widget.ProgressBar;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;

/**
 * Created by fvalle on 14/10/2015.
 * ViewHolder que muestra un banner en los items de portada
 */
public class BasePortadaItemBannerVH extends BasePortadaItemHolder {

    //private SASBannerView bannerView;
    private ProgressBar spinner;

    private int siteId = 50295;
    private String pageId = "605927";
    private int formatId = 36692;

    public BasePortadaItemBannerVH(View itemView) {
        super(itemView);
        spinner = (ProgressBar) itemView.findViewById(R.id.progressBar);
        //bannerView = (SASBannerView) itemView.findViewById(R.id.banner);
        //SASAdView.enableLogging();
    }

    @Override
    public void setItem(final Nodo item) {

        super.setItem(item);

        spinner.setVisibility(View.VISIBLE);
        //bannerView.loadAd(getSiteId(), getPageId(), getFormatId(), true, "", adResponseHandler);
    }

    /*SASAdView.AdResponseHandler adResponseHandler = new SASAdView.AdResponseHandler() {

        public void adLoadingCompleted(SASAdElement sasAdElement) {
            // the Ad is fully loaded
            // BEWARE : this method will be executed from a thread other than the Main Android thread.
            // THEREFORE, any code that needs to be executed from the Main thread (like showing/hiding/resizing a banner
            // MUST be wrapped in a Runnable Object which will be executed on the Main thread.
            // This can be done using the convenience method of SASAdView: mBannerView.executeOnUIThread(runnable)
            Util.scanForActivity(context).runOnUiThread(new Runnable() {
                public void run() {
                    spinner.setVisibility(View.GONE);
                }
            });
        }

        public void adLoadingFailed(final Exception e) {
            // the Ad failed to load
            // BEWARE : this method will ALSO be executed from a thread other than the Main Android thread, so follow
            // recommendations that apply for adLoadingCompleted method.

            Util.scanForActivity(context).runOnUiThread(new Runnable() {
                public void run() {
                    spinner.setVisibility(View.GONE);
                    Log.e("Smart ad server", e.getMessage());
                }
            });
        }
    };*/

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getFormatId() {
        return formatId;
    }

    public void setFormatId(int formatId) {
        this.formatId = formatId;
    }
}
