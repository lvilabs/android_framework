package ar.com.lavoz.lavozbase.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.apache.commons.lang3.ObjectUtils;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.service.GcmService;
import ar.com.lavoz.lavozbase.notifications.Notification;
import roboguice.activity.RoboActivity;

/**
 * Created by desarrollo on 23/2/16.
 *
 */
public class BaseActivity extends RoboActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
