package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.LinkedHashMap;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.util.Util;

/**
 * Created by fvalle on 19/10/2015.
 *
 */
public class PortadaItemOpinionVH extends BasePortadaItemHolder {

    private TextView titulo;
    private TextView bajada;
    private TextView nombreColumnista;
    private TextView cargoColumnista;
    private ImageView imagen;
    private LinearLayout cabecera;


    public PortadaItemOpinionVH(View itemView){
        super(itemView);
    }

    @Override
    public void setItem(Nodo item) {

        super.setItem(item);

        titulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        bajada = (TextView) itemView.findViewById(R.id.txtBajada);
        imagen = (ImageView) itemView.findViewById(R.id.imgImagen);
        nombreColumnista  = (TextView) itemView.findViewById(R.id.txtAutorNombre);
        cargoColumnista  = (TextView) itemView.findViewById(R.id.txtAutorCargo);
        cabecera = (LinearLayout)itemView.findViewById(R.id.cabecera);

        if(item.getPrimerItem())
            cabecera.setVisibility(View.VISIBLE);
        else
            cabecera.setVisibility(View.GONE);

        titulo.setText(item.getTitulo());
        bajada.setText(item.getBajadaTexto());
        if(item.getAutor() != null) {
            LinkedHashMap autor = item.getAutor();
            ImageLoader.getInstance().displayImage(Util.getAutorImageUrl(item, autor), imagen);
            nombreColumnista.setText(autor.get("name") != null ? autor.get("name").toString() : "");
            cargoColumnista.setText(autor.get("sobremi") != null ? autor.get("sobremi").toString() : "");
        }
    }

}
