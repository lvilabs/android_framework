package ar.com.lavoz.lavozbase.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import ar.com.lavoz.lavozbase.R;
import ar.com.lavoz.lavozbase.view.ResultView;
import ar.com.lavoz.lavozbase.view.implement.PortadillaViewImpl;
import ar.com.lavoz.lavozbase.view.implement.ResultViewImpl;

/**
 * Created by desarrollo on 8/3/16.
 *
 */
public class Notification {

    private static Notification notification;

    private Notification() {

    }

    public static Notification getInstance() {
        if (notification == null) {
            notification = new Notification();
        }
        return notification;
    }

    public void showNotification(Context context, int id, String title, String text) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.logo_lavoz)
                        .setContentTitle(title)
                        .setContentText(text);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, ResultViewImpl.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ResultViewImpl.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // id allows you to update the notification later on.
        mNotificationManager.notify(id, mBuilder.build());

    }
}
