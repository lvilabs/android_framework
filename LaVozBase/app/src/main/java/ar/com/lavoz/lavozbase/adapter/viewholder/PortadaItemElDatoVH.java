package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ar.com.lavoz.noticiasservice.model.Nodo;
import ar.com.lavoz.lavozbase.R;


/**
 * Created by fvalle on 14/10/2015.
 */
public class PortadaItemElDatoVH extends BasePortadaItemHolder {

    private TextView titulo;
    private TextView bajada;
    private TextView tagDuro;
    private LinearLayout cabecera;

    public PortadaItemElDatoVH(View itemView) {

        super(itemView);
    }

    @Override
    public void setItem(Nodo item){

        super.setItem(item);

        titulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        bajada = (TextView) itemView.findViewById(R.id.txtBajada);
        tagDuro  = (TextView) itemView.findViewById(R.id.txtTagDuro);
        cabecera = (LinearLayout) itemView.findViewById(R.id.cabecera);

        tagDuro.setText(item.getTag_duro() != null ? item.getTag_duro().getName() : "");
        bajada.setText(item.getBajada());
        titulo.setText(item.getTitulo());
        if(item.getPrimerItem()){
            cabecera.setVisibility(View.VISIBLE);
        }else{
            cabecera.setVisibility(View.GONE);
        }
    }
}
