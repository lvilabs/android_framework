package ar.com.lavoz.lavozbase.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

/**
 * Created by fvalle on 14/10/2015.
 * Interface que represent ala vista de portadilla
 *
 */
public interface PortadillaView {

    RecyclerView getRecycler();

    void setRecycler(RecyclerView recycler);

    ProgressBar getSpinner();

    void setSpinner(ProgressBar spinner);

    Context getContext();

    String getTid();

    void setTid(String tid);

}
