package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;

import ar.com.lavoz.noticiasservice.model.Nodo;

/**
 * Created by fvalle on 06/11/2015.
 * ViewHolder Para el primer banner de portada
 */
public class PortadaItemBanner2 extends BasePortadaItemBannerVH {

    private static Boolean esNuevaVista;

    public PortadaItemBanner2(View itemView) {
        super(itemView);
        // SiteID / PageID / FormatID: 50295 / 605927 / 37070
        setSiteId(50295);
        setPageId("605927");
        setFormatId(37070);
        esNuevaVista = true;
    }

    @Override
    public void setItem(Nodo item) {
        if(esNuevaVista) {
            super.setItem(item);
            esNuevaVista = false;
        }
    }
}
