package ar.com.lavoz.lavozbase.adapter.viewholder;

import android.view.View;

import ar.com.lavoz.noticiasservice.model.Nodo;

/**
 * Created by fvalle on 05/11/2015.
 * Viewholder de un item de la portada que muestra el bloque Agroverdad
 */
public class PortadaItemAgroverdadVH extends BasePortadaItemHolder {

    public PortadaItemAgroverdadVH(View itemView){
        super(itemView);
    }

    @Override
    public void setItem(Nodo item) {

        super.setItem(item);
    }

    @Override
    public void onClick(View v) {
        
    }
}
