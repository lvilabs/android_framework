package ar.com.lavoz.lavozbase.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.orm.SugarContext;

import java.util.List;
import java.util.Random;

import ar.com.lavoz.framework.model.dto.Preferencia;
import ar.com.lavoz.framework.service.PreferenciaService;

/**
 * Created by Dani on 5/4/2016.
 */
public class PreferenciaServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "PREFERENCIASERVICETEST:";

    public PreferenciaServiceTest() {
        super(Application.class);
    }

    public void testInicializar() throws InterruptedException {
        UtilTest.inicializarFramework(getContext().getApplicationContext());
        SugarContext.init(getContext().getApplicationContext());
        PreferenciaService preferenciaService = UtilTest.appFrameworkContext.getPreferenciaService();

        preferenciaService.inicializarPreferencias();
        List<Preferencia> preferencias = preferenciaService.obtenerPreferencias();

        mostrarPreferencias(preferencias);
        assertTrue(preferencias.size() > 0);

        //UtilTest.appFrameworkContext.sqlLiteTerminate();
        SugarContext.terminate();
    }

    public void testModificar() throws InterruptedException {
        UtilTest.inicializarFramework(getContext().getApplicationContext());
        SugarContext.init(getContext().getApplicationContext());
        PreferenciaService preferenciaService = UtilTest.appFrameworkContext.getPreferenciaService();

        List<Preferencia> preferencias = preferenciaService.obtenerPreferencias();


        int randomIndex = new Random().nextInt(9 - 1) + 1;
        Preferencia p = preferencias.get(randomIndex);

        Log.i(LOG_TAG, "Modificar - preferencia("+randomIndex+") ----> "+ p.toString());
        p.setValor(true);

        Log.i(LOG_TAG, "Grabando preferencias...");
        preferenciaService.grabar(preferencias);

        preferencias = preferenciaService.obtenerPreferencias();

        p = preferencias.get(randomIndex);

        assertTrue(p.getValor());
        mostrarPreferencias(preferencias);

        SugarContext.terminate();
    }

    private void mostrarPreferencias(List<Preferencia> preferencias) {
        Log.i(LOG_TAG, "--------------- PREFERENCIAS ---------------");
        for (Preferencia preferencia: preferencias) {
            Log.i(LOG_TAG, preferencia.toString());
        }
        Log.i(LOG_TAG, "--------------------------------------------");
    }
}
