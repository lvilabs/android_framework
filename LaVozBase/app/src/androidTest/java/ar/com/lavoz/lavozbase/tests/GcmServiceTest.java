package ar.com.lavoz.lavozbase.tests;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.framework.gcm.RegistrationIntentService;
import ar.com.lavoz.framework.service.GcmService;


/**
 * Created by Dani on 3/3/2016.
 */
public class GcmServiceTest extends ApplicationTestCase<Application> {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String LOG_TAG = "GCMSERVICETEST:";
    CountDownLatch signal = new CountDownLatch(1);
    GcmService.OnResultListener gcmServiceListener = new GcmService.OnResultListener() {
        @Override
        public void successByTopic(Bundle data) {
            Log.i(LOG_TAG, "SUCCESS BY TOPIC!!!");
            Log.i(LOG_TAG, "Message: " + data.toString());
            assertEquals("Mensaje Global!!!", data.get("message"));
            signal.countDown();
        }

        @Override
        public void successByDeviceID(Bundle data) {
            Log.i(LOG_TAG, "SUCCESS BY ID!!!");
            Log.i(LOG_TAG, "Message: " + data.toString());
            signal.countDown();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);

            signal.countDown();
        }
    };
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public GcmServiceTest() {
        super(Application.class);
    }

    public void testObtenerNotificacion() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        GcmService gcmService = UtilTest.appFrameworkContext.getGcmService();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(RegistrationIntentService.SENT_TOKEN_TO_SERVER, false);
                Log.i(LOG_TAG, ">> COMPLETE_REGISTER ::: sentToken ::: " + sentToken);
            }
        };

        try {
            Log.d(LOG_TAG, "----->>>>> DEBUG 1");
            // Add topic to listen.
            gcmService.addTopic("global");
            gcmService.addTopic("deportes");

            // Add the listener service.
            gcmService.addListener(gcmServiceListener);
            Log.d(LOG_TAG, "----->>>>> DEBUG 2");

            // Registering BroadcastReceiver
            gcmService.registerReceiver(mRegistrationBroadcastReceiver);
            Log.d(LOG_TAG, "----->>>>> DEBUG 13");

            // Start the service.
            gcmService.startService();
            Log.d(LOG_TAG, "----->>>>> DEBUG 4");

            UtilTest.sendGCMMessage("/topics/global", "Mensaje Global!!!");
            Log.d(LOG_TAG, "----->>>>> DEBUG 5");

            signal.await(5, TimeUnit.SECONDS);// Esperar la respuesta.
            Log.d(LOG_TAG, "----->>>>> DEBUG 6");
            gcmService.stopService();
        } catch (Exception e) {
            Log.e(LOG_TAG, ">> ERROR " + e.getMessage());
            assertTrue(false);
            e.printStackTrace();
        }
    }

}
