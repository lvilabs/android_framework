package ar.com.lavoz.lavozbase.tests;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import ar.com.lavoz.framework.context.AppFrameworkContext;
import ar.com.lavoz.framework.exception.LVIException;
import ar.com.lavoz.noticiasservice.context.NoticiasContext;

/**
 * Clase de utilidad para los test.
 */
public class UtilTest {
    private static final String LOG_TAG = "UTIL-TEST:";
    private static final String GCM_SEND_ENDPOINT = "https://gcm-http.googleapis.com/gcm/send";

    public static AppFrameworkContext appFrameworkContext;
    public static NoticiasContext noticiasContext;

    public static void inicializarFramework(Context context) {
        if (appFrameworkContext == null) {
            try {
                appFrameworkContext = new AppFrameworkContext(context);
                noticiasContext = new NoticiasContext(context);
            } catch (LVIException e) {
                Log.i(LOG_TAG, e.getMessage());
            }
        }
    }

    /**
     * Send a downstream message via HTTP JSON.
     *
     * @param destination the registration id of the recipient app.
     * @param message     the message to be sent
     * @throws IOException
     */
    public static String sendGCMMessage(String destination,
                                        String message) throws IOException {

        String json = "{ \"to\":\"" + destination + "\"," +
                "\"data\":{" +
                "       \"message\":\"" + message + "\"" +
                "}}";

        Log.i(LOG_TAG, "JSON::::" + json);

        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setHeaders();
        httpRequest.doPost(GCM_SEND_ENDPOINT, json);

        if (httpRequest.getResponseCode() != 200) {
            throw new IOException("Invalid request."
                    + " status: " + httpRequest.getResponseCode()
                    + " response: " + httpRequest.getResponseBody());
        }

        JSONObject jsonResponse;
        try {
            jsonResponse = new JSONObject(httpRequest.getResponseBody());
            Log.i("Util-Test", "Send message:\n" + jsonResponse.toString(2));
        } catch (JSONException e) {
            Log.i("Util-Test", "Failed to parse server response:\n"
                    + httpRequest.getResponseBody());
        }
        return httpRequest.getResponseBody();
    }

    public static void copyFileToSD(String currentDBPath, String backupDBPath) {
        try {
            Log.e("Util-Test", "COPIANDO ARCHIVO A SD: " + currentDBPath);
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                //String currentDBPath = "/data/" + getPackageName() + "/databases/yourdatabasename";
                //String backupDBPath = "backupname.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {
            Log.e("Util-Test", "Failed copy file to SD:\n" + e.getMessage());
        }
    }

}
