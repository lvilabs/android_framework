package ar.com.lavoz.lavozbase.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.orm.SugarContext;

import java.util.List;

import ar.com.lavoz.framework.model.dto.Usuario;
import ar.com.lavoz.framework.service.UsuarioDbService;

/**
 * Created by Dani on 2/18/2016.
 */
public class UsuarioDbServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "USUARIODBSERVICETEST:";

    public UsuarioDbServiceTest() {
        super(Application.class);
    }

    public void testUsuario() throws InterruptedException {
        UtilTest.inicializarFramework(getContext().getApplicationContext());

        SugarContext.init(getContext().getApplicationContext());
        //UtilTest.appFrameworkContext.sqlLiteInit();
        UsuarioDbService usuarioDbService = UtilTest.appFrameworkContext.getUsuarioDbService();

        // Limpiar la base para empezar.
        limpiarBase(usuarioDbService);

        Usuario user = crearUsuario("ccc", "ddd", "eee", "info@info.com", "22222", "M");
        Log.e(LOG_TAG, "Grabando el usuario!!!.");
        long dbId = usuarioDbService.grabar(user);
        Log.e(LOG_TAG, "Usuario grabado -- ID: " + dbId);

        Usuario resultUser = usuarioDbService.obtener(dbId);

        Log.i(LOG_TAG, "ID: " + resultUser.getId());

        assertEquals(user.getNombre(), resultUser.getNombre());
        assertEquals(user.getApellido(), resultUser.getApellido());
        assertEquals(user.getDireccion(), resultUser.getDireccion());
        assertEquals(user.getEmail(), resultUser.getEmail());
        assertEquals(user.getTelefono(), resultUser.getTelefono());
        assertEquals(user.getSexo(), resultUser.getSexo());

        assertTrue(usuarioDbService.borrar(dbId));

        Log.e(LOG_TAG, "Usuario borrado -- ID: " + dbId);

        //UtilTest.appFrameworkContext.sqlLiteTerminate();
        SugarContext.terminate();
    }

    public void testUsuarioLista() throws InterruptedException {
        UtilTest.inicializarFramework(getContext().getApplicationContext());

        UtilTest.appFrameworkContext.sqlLiteInit();
        UsuarioDbService usuarioDbService = UtilTest.appFrameworkContext.getUsuarioDbService();

        // Limpiar la base para empezar.
        limpiarBase(usuarioDbService);

        Usuario user1 = crearUsuario("ccc1", "ddd1", "eee1", "info1@info.com", "11111", "M");
        Usuario user2 = crearUsuario("ccc2", "ddd2", "eee2", "info2@info.com", "22222", "F");

        long dbId1 = usuarioDbService.grabar(user1);
        long dbId2 = usuarioDbService.grabar(user2);

        List<Usuario> usuarios1 = usuarioDbService.obtenerTodos();
        Log.i(LOG_TAG, "Cantidad de usuarios en la base(obtenerTodos): " + usuarios1.size());

        List<Usuario> usuarios2 = usuarioDbService.buscar("nombre = ?", "ccc1");
        Log.i(LOG_TAG, "Cantidad de usuarios en la base(buscar): " + usuarios2.size());

        assertEquals(usuarios1.size(), 2);
        assertEquals(usuarios2.size(), 1);
        assertTrue("Eliminar usuario 1", usuarioDbService.borrar(dbId1));
        assertTrue("Eliminar usuario 2", usuarioDbService.borrar(dbId2));

        UtilTest.appFrameworkContext.sqlLiteTerminate();
    }

    private void limpiarBase(UsuarioDbService service) {
        Log.i(LOG_TAG, " --- Limpiar base de datos ---");

        List<Usuario> usuarios = service.obtenerTodos();
        for (Usuario usuario : usuarios) {
            Log.i(LOG_TAG, " *** Limpiar usuario :::" + usuario.getId());
            service.borrar(usuario.getId());
        }
    }

    private Usuario crearUsuario(String nombre, String apellido, String direccion, String email, String telefono, String sexo) {
        Usuario user = new Usuario();

        user.setNombre(nombre);
        user.setApellido(apellido);
        user.setDireccion(direccion);
        user.setEmail(email);
        user.setSexo(sexo);
        user.setTelefono(telefono);

        return user;
    }
}
