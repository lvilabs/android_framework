package ar.com.lavoz.lavozbase.tests;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.util.concurrent.CountDownLatch;

import ar.com.lavoz.framework.exception.LVIServiceException;
import ar.com.lavoz.noticiasservice.NodoService;
import ar.com.lavoz.noticiasservice.model.Nodo;

/**
 * Created by Dani on 2/3/2016.
 */
public class NodoServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "NOTASERVICETEST:";
    static CountDownLatch signal = new CountDownLatch(1);

    public NodoServiceTest() {
        super(Application.class);
    }

    public void testObtenerNodo() throws InterruptedException {
        UtilTest.inicializarFramework(getContext());
        NodoService nodoService = UtilTest.noticiasContext.getNodoService(notaServiceListener);

        // Primera llamada(Sin Cache).
        nodoService.obtenerNodo("nid1038186");
        signal.await();// Esperar para la primera llamada.

        // Segunda llamada(Con Cache).
        signal = new CountDownLatch(1);
        //nodoService.obtenerNodo("nid1053101");
        nodoService.obtenerNodo("nid1038186");
        signal.await();// Esperar para la segunda llamada.
    }

    // Definimos los oyentes a cada llamada del servicio.
    NodoService.OnResultListener notaServiceListener = new NodoService.OnResultListener() {
        @Override
        public void success(Nodo unNodo) {
            Log.i(LOG_TAG, "SUCCESS!!!");
            assertNotNull(unNodo);

            Log.i(LOG_TAG, "SID: " + unNodo.getApi().sid);
            Log.i(LOG_TAG, "BODY TEXTO: " + unNodo.getBodyTexto());

            signal.countDown();
        }

        @Override
        public String toString() {
            return super.toString();
        }

        @Override
        public void failure(LVIServiceException lviEx) {
            Log.i(LOG_TAG, "FAILURE!!!");
            Log.i(LOG_TAG, lviEx.getMessage());
            assertTrue(false);
            signal.countDown();
        }
    };

}
