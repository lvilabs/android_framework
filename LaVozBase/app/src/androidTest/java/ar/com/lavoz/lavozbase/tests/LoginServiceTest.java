package ar.com.lavoz.lavozbase.tests;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.orm.SugarContext;

import ar.com.lavoz.framework.model.LoginResult;
import ar.com.lavoz.framework.service.LoginService;

/**
 * Created by Dani on 4/6/2016.
 */
public class LoginServiceTest extends ApplicationTestCase<Application> {
    private static final String LOG_TAG = "LOGINTEST:";

    public LoginServiceTest() {
        super(Application.class);
    }

    public void testRegisterLoginLogout() throws InterruptedException {
        UtilTest.inicializarFramework(getContext().getApplicationContext());
        LoginService loginService = UtilTest.appFrameworkContext.getLoginService();
        //UtilTest.appFrameworkContext.sqlLiteInit();
        SugarContext.init(getContext().getApplicationContext());

        loginService.clearSessions("panello");
        loginService.clearSessions("dsandoval");

        // Copiar la base de datos al SD.
        UtilTest.copyFileToSD("//data//" + getContext().getPackageName() + "//databases//app_framework_0", "backup_app_framework_0.db");

        loginService.register("dsandoval", "daniel1");

        LoginResult lResult = loginService.login("panello", "pablo1");
        if (lResult != null) {
            loginService.logout(lResult.getResponse().getToken());
        }

        //UtilTest.appFrameworkContext.sqlLiteTerminate();
        SugarContext.terminate();
    }

}
