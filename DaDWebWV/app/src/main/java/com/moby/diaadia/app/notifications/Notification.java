package com.moby.diaadia.app.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.moby.diaadia.app.R;
import com.moby.diaadia.app.app;
import com.moby.diaadia.app.model.NotificationData;
import com.moby.diaadia.app.util.Util;
import com.moby.diaadia.app.views.implement.MainActivityImpl;

/**
 * Created by desarrollo on 8/3/16.
 *
 */
public class Notification {

    public static final String EXTRA_DATA = "data";

    private static Notification notification;

    private Notification() {

    }

    public static Notification getInstance() {
        if (notification == null) {
            notification = new Notification();
        }
        return notification;
    }

    public void showNotification(Context context, NotificationData notificationData) {

        // Create remote view and set bigContentView.
        RemoteViews expandedView = new RemoteViews(context.getPackageName(),
                R.layout.custom_notification);
        expandedView.setTextViewText(R.id.notification_title, notificationData.getTitle());
        Bitmap bmp = Util.getBitmapFromURL(notificationData.getImage());
        if(bmp != null) {
            //mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bmp));
            expandedView.setImageViewBitmap(R.id.notification_image, bmp);
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_logo)
                        .setContentTitle(notificationData.getTitle())
                        .setContentText(notificationData.getText())
                        .setColor(ContextCompat.getColor(context, R.color.colorRed))
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationData.getTitle()))
                        .setAutoCancel(true);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivityImpl.class);

        resultIntent.putExtra(EXTRA_DATA , notificationData);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivityImpl.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        android.app.Notification notification = mBuilder.build();
        if(bmp != null) {
            notification.bigContentView = expandedView;
        }

        try {
            ((app)context.getApplicationContext()).trackEvent( "notificacion", "notificacion-recibida", notificationData.getText() );
        }catch (Exception e){
            e.printStackTrace();
            Log.w("Notificacion", e.getMessage());
        }

        // id allows you to update the notification later on.
        mNotificationManager.notify(notificationData.getNid() , notification);

    }
}
