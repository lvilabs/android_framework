package com.moby.diaadia.app.views;

import android.content.Context;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/**
 * Created by fvalle on 21/03/2016.
 *
 */
public interface MainActivity {
    public WebView getWebView();

    Button getBtnReintentar();
    void setBtnReintentar(Button btnReintentar);
    FrameLayout getSplash();
    void setSplash(FrameLayout splash);
    LinearLayout getTxtError();
    void setTxtError(LinearLayout txtError);
    void setWebView(WebView webView);
    Context getContext();
    Button getReload();
    void setReload(Button reload);
    void close();
}
