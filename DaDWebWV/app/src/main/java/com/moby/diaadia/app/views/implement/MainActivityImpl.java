package com.moby.diaadia.app.views.implement;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.moby.diaadia.app.R;
import com.moby.diaadia.app.model.NotificationData;
import com.moby.diaadia.app.notifications.Notification;
import com.moby.diaadia.app.presenter.MainPresenter;
import com.moby.diaadia.app.views.MainActivity;

public class MainActivityImpl extends AppCompatActivity implements MainActivity {

    private WebView webView;
    private MainPresenter presenter;
    private LinearLayout txtError;
    private Button btnReintentar;
    private FrameLayout splash;


    public MainActivityImpl()
    {

    }

    @Override
    public Button getReload() {
        return reload;
    }

    @Override
    public void setReload(Button reload) {
        this.reload = reload;
    }

    private Button reload;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getWebView().saveState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setWebView((WebView) findViewById(R.id.webView));
        setTxtError((LinearLayout) findViewById(R.id.txtError));
        setBtnReintentar((Button) findViewById(R.id.btnReintentar));
        setSplash((FrameLayout) findViewById(R.id.splash));
        setReload((Button) findViewById(R.id.reload));

        presenter = new MainPresenter(this);
        NotificationData notificationData = null;
        if (savedInstanceState != null) {
            getWebView().restoreState(savedInstanceState);
            presenter.setPageSaved(true);
        }else {

            presenter.setPageSaved(false);
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                notificationData = (NotificationData) extras.getSerializable(Notification.EXTRA_DATA);
            }
        }

        presenter.mostrarDatos(notificationData);
    }

    @Override
    public FrameLayout getSplash() {
        return splash;
    }

    @Override
    public void setSplash(FrameLayout splash) {
        this.splash = splash;
    }

    @Override
    public Button getBtnReintentar() {
        return btnReintentar;
    }

    @Override
    public void setBtnReintentar(Button btnReintentar) {
        this.btnReintentar = btnReintentar;
    }

    @Override
    public LinearLayout getTxtError() {
        return txtError;
    }

    @Override
    public void setTxtError(LinearLayout txtError) {
        this.txtError = txtError;
    }

    @Override
    public WebView getWebView() {
        return webView;
    }

    @Override
    public void setWebView(WebView webView) {
        this.webView = webView;
    }

    @Override
    public Context getContext(){
        return MainActivityImpl.this;
    }

    @Override
    public void close(){
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            presenter.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
